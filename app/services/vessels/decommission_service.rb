# frozen_string_literal: true

module Vessels
  class DecommissionService < ::BaseService
    def run_execute
      vessel.decommission!

      success(vessel)
    end

    private

    def vessel
      Vessel.for_user(user).find(params[:vessel_id])
    end

    def success_key
      :vessel
    end
  end
end
