# frozen_string_literal: true

module Vessels
  class EditService < ::BaseService
    def run_execute
      vessel.update!(
        id: params[:vessel_id],
        **params.except(:vessel_id)
      )

      success(vessel)
    end

    private

    def vessel
      Vessel.for_user(user).find(params[:vessel_id])
    end

    def success_key
      :vessel
    end
  end
end
