# frozen_string_literal: true

module Vessels
  class CreateService < ::BaseService
    def run_execute
      success(create_vessel)
    end

    private

    def create_vessel
      Vessel.create!(**params, household: user.household)
    end

    def success_key
      :vessel
    end
  end
end
