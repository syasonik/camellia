# frozen_string_literal: true

module Households
  class MergeService < ::BaseService
    def run_execute
      new_household.merge_household!(old_household)

      success(new_household)
    end

    private

    def new_household
      Household.find(params[:household_id])
    end

    def old_household
      user.household
    end

    def success_key
      :household
    end
  end
end
