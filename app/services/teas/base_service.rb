# frozen_string_literal: true

module Teas
  class BaseService < ::BaseService
    protected

    def tea
      @tea ||= Tea.for_user(user).find(params[:tea_id])
    end

    def vessel_for(id)
      Vessel.for_user(user).find(id) if id
    end

    def escalation_profiles
      raw_profiles.map { |profile| escalation_profile_for(profile) }
    end

    def raw_profiles
      @raw_profiles ||= Array(params[:escalation_profiles])
    end

    def escalation_profile_for(profile)
      EscalationProfile.new(attributes_for_profile(profile))
    end

    def attributes_for_profile(profile)
      {
        vessel: vessel_for(profile[:vessel_id]),
        escalations: escalations_for(profile[:escalations])
      }.compact
    end

    def escalations_for(escalations)
      escalations&.map { |escalation| Escalation.new(escalation) }
    end

    def success_key
      :tea
    end
  end
end
