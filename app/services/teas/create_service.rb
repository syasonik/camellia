# frozen_string_literal: true

module Teas
  class CreateService < BaseService
    def run_execute
      success Tea.create!(attributes_for_tea)
    end

    private

    def attributes_for_tea
      {
        **params.slice(:name, :description),
        escalation_profiles: escalation_profiles,
        household: user.household
      }
    end
  end
end
