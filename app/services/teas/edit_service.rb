# frozen_string_literal: true

module Teas
  class EditService < BaseService
    def run_execute
      raise_if_duplicate_profiles!
      raise_if_nonexistant_ids!
      delete_obsolete_profiles!

      tea.update!(attributes_for_tea)

      success tea
    end

    private

    def attributes_for_tea
      {
        **params.slice(:name, :description, :decommissioned),
        escalation_profiles: escalation_profiles
      }.compact
    end

    # @return [Array<EscalationProfile>]
    def escalation_profiles
      raw_profiles
        .map { |profile| update_or_initialize_profile(profile) }
        .tap { |profiles| raise_if_invalid_profiles!(profiles) }
    end

    # @param profile [Hash]
    # @return [EscalationProfile]
    def update_or_initialize_profile(profile)
      update_existing_profile!(profile) || escalation_profile_for(profile)
    end

    # @param profile [Hash]
    # @return [EscalationProfile]
    def update_existing_profile!(profile)
      profile_id = get_profile_id(profile)
      existing_profile = find_existing_profile(profile_id)

      return unless existing_profile

      vessel_id = vessel_for(profile[:vessel_id])&.id
      escalations = Array(profile[:escalations])

      existing_profile.tap do |existing|
        existing.vessel_id = vessel_id
        add_new_escalations(existing, escalations)
      end
    end

    # @param existing_profile [EscalationProfile]
    # @param escalations [Array<Hash>]
    # @return unused
    def add_new_escalations(existing_profile, escalations)
      raise_if_duplicate_escalations!(escalations)
      delete_escalations!(existing_profile)

      escalations.each { |escalation| existing_profile.escalations.build(escalation) }
    end

    # @param profile [Hash]
    # @return [Hash]
    def attributes_for_profile(profile)
      super.merge(tea: tea)
    end

    # @return [Array<Integer>] unused
    def delete_obsolete_profiles!
      obsolete_profile_ids = existing_profiles_by_id.keys - retained_profiles_by_id.keys

      obsolete_profile_ids.each { |id| find_existing_profile(id).destroy! }
    end

    # @param profile [EscalationProfile]
    # @return [ActiveRecord::Collection<Escalation>] unused
    def delete_escalations!(profile)
      profile.escalations.each(&:destroy!)
    end

    # @param profiles [Array<EscalationProfile>]
    def raise_if_invalid_profiles!(profiles)
      profiles.each.with_index { |profile, idx| validate_profile(profile, idx) }

      raise ActiveRecord::RecordInvalid, tea if tea.errors.any?
    end

    # Validate profiles/escalations in advance of save, to
    # retain indexing and error granularity. `autosave` on
    # create preserves indexes, but does not for new records
    # added in associations on update.
    #
    # @param profile [EscalationProfile]
    # @param idx [Integer]
    # @return unused
    def validate_profile(profile, idx)
      return if profile.valid?

      profile.errors.each { |error| tea.errors.add(:"escalation_profiles[#{idx}].#{error.attribute}", error.message) }
    end

    def raise_if_duplicate_profiles!
      return unless duplicates?(raw_profiles, by: :vessel_id)

      raise 'Teas may only have one escalation profile per vessel and a default profile'
    end

    # @param escalations [Hash]
    def raise_if_duplicate_escalations!(escalations)
      return unless duplicates?(escalations, by: :rank)

      raise 'Escalation profiles may only have one escalation per rank'
    end

    def raise_if_nonexistant_ids!
      nonexistant_ids = retained_profiles_by_id.keys - existing_profiles_by_id.keys

      raise "Couldn't find EscalationProfiles with 'id' #{nonexistant_ids.join(', ')}" if nonexistant_ids.any?
    end

    # @return [Hash<Integer, EscalationProfile>]
    def retained_profiles_by_id
      @retained_profiles_by_id ||= raw_profiles.index_by { |profile| get_profile_id(profile) }.compact
    end

    # @return [Hash<Integer, EscalationProfile>]
    def existing_profiles_by_id
      @existing_profiles_by_id ||= tea.escalation_profiles.includes(:escalations).index_by(&:id)
    end

    # @param collection [Enumerable]
    # @param by [Array<Symbol>]
    # @return [Boolean]
    def duplicates?(collection, by: [])
      uniq_collection = collection.uniq { |profile| profile.slice(*by).compact }

      collection.length != uniq_collection.length
    end

    # @param profile [Hash]
    # @return [Integer]
    def get_profile_id(profile)
      profile[:escalation_profile_id]&.to_i
    end

    # @param profile_id [Integer]
    # @return [EscalationProfile]
    def find_existing_profile(profile_id)
      existing_profiles_by_id[profile_id]
    end
  end
end
