# frozen_string_literal: true

module Teas
  class DecommissionService < BaseService
    def run_execute
      tea.decommission!

      success tea
    end
  end
end
