# frozen_string_literal: true

module Servings
  class CreateService < ::BaseService
    attr_reader :serving

    def run_execute
      serving = create_serving
      tea.use_serving!
      serving.vessel.set_serving!(serving)

      success(serving)
    end

    private

    def create_serving
      Serving.create!(
        tea: tea,
        vessel: vessel,
        is_finished: false
      )
    end

    def tea
      Tea.for_user(user).find(params[:tea_id])
    end

    def vessel
      Vessel.for_user(user).find(params[:vessel_id])
    end

    def success_key
      :serving
    end
  end
end
