# frozen_string_literal: true

module Servings
  class TrashService < ::BaseService
    def run_execute
      serving.trash!

      success(serving)
    end

    private

    def serving
      Serving.for_user(user).find(params[:serving_id])
    end

    def success_key
      :serving
    end
  end
end
