# frozen_string_literal: true

class BaseService
  attr_reader :params, :user

  def initialize(user, params)
    @user = user
    @params = params
  end

  def execute
    ActiveRecord::Base.transaction do
      run_execute
    end
  rescue StandardError => e
    Rails.logger.info e.message
    Rails.logger.info e.backtrace
    error(e.message)
  end

  def run_execute
    raise NotImplementedError
  end

  protected

  # Override in subclass
  def success_key
    raise NotImplementedError
  end

  def success(result)
    {
      status: :success,
      type: success_key,
      success_key => result
    }
  end

  def error(message)
    {
      status: :error,
      type: success_key,
      message: message
    }
  end
end
