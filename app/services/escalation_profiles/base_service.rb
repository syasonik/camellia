# frozen_string_literal: true

module EscalationProfiles
  class BaseService < ::BaseService
    private

    def vessel(id)
      Vessel.for_user(user).find(id) if id
    end

    def tea(id)
      Tea.for_user(user).find(id)
    end

    def escalations(escalations)
      escalations&.map { |escalation| Escalation.new(escalation) }
    end

    def success_key
      :escalation_profile
    end
  end
end
