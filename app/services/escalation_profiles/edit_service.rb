# frozen_string_literal: true

module EscalationProfiles
  class EditService < BaseService
    def run_execute
      escalation_profile.update!(profile_params)

      success escalation_profile
    end

    private

    def escalation_profile
      @escalation_profile ||= EscalationProfile.for_user(user).find(params[:escalation_profile_id])
    end

    def profile_params
      {
        escalations: escalations(params[:escalations]),
        vessel_id: vessel(params[:vessel_id])&.id,
        tea_id: (tea(params[:tea_id]).id if params[:tea_id])
      }.compact
    end

    # @param existing_profile [EscalationProfile]
    # @param escalations [Array<Hash>]
    # @return unused
    def escalations(escalations)
      return unless params[:escalations]

      raise_if_duplicate_escalations!(escalations)
      delete_escalations!

      super
    end

    # @param escalations [Hash]
    def raise_if_duplicate_escalations!(escalations)
      return unless duplicates?(escalations, by: :rank)

      raise 'Escalation profiles may only have one escalation per rank'
    end

    # @param profile [EscalationProfile]
    # @return [ActiveRecord::Collection<Escalation>] unused
    def delete_escalations!
      escalation_profile.escalations.each(&:destroy!)
    end

    # @param collection [Enumerable]
    # @param by [Array<Symbol>]
    # @return [Boolean]
    def duplicates?(collection, by: [])
      uniq_collection = collection.uniq { |profile| profile.slice(*by).compact }

      collection.length != uniq_collection.length
    end
  end
end
