# frozen_string_literal: true

module EscalationProfiles
  class CreateService < BaseService
    def run_execute
      success(create_escalation_profile)
    end

    private

    def create_escalation_profile
      EscalationProfile.for_user(user).create!(profile_params)
    end

    def profile_params
      {
        escalations: escalations(params[:escalations]),
        vessel: vessel(params[:vessel_id]),
        tea: tea(params[:tea_id])
      }.compact
    end
  end
end
