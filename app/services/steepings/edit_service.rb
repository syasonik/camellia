# frozen_string_literal: true

module Steepings
  class EditService < ::BaseService
    def run_execute
      steeping.update!(
        id: params[:steeping_id],
        **params.except(:steeping_id)
      )

      success(steeping)
    end

    private

    def steeping
      Steeping.for_user(user).find(params[:steeping_id])
    end

    def success_key
      :steeping
    end
  end
end
