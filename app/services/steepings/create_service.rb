# frozen_string_literal: true

module Steepings
  class CreateService < ::BaseService
    def run_execute
      success(create_steep)
    end

    private

    def create_steep
      Steeping.create!(
        serving: serving,
        **recommended_steep_params,
        **overriden_steep_params
      )
    end

    def serving
      Serving.for_user(user).find(params[:serving_id])
    end

    def recommended_steep_params
      serving.recommended_steep_params
    end

    def overriden_steep_params
      params.slice(:duration, :temperature, :is_good).compact
    end

    def success_key
      :steeping
    end
  end
end
