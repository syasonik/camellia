# frozen_string_literal: true

class Steeping < ApplicationRecord
  belongs_to :serving
  has_one :tea, through: :serving

  validates :duration, numericality: { only_integer: true, greater_than: 0 }, presence: true
  validates :temperature, inclusion: 1..100
  validates :steep_number, numericality: { only_integer: true, greater_than: 0 },
                           uniqueness: { scope: :serving,
                                         message: 'should increment steep number by one' }
  validates :is_good, inclusion: { in: [true, false] }, allow_nil: true

  scope :for_user, ->(user) { joins(:tea).merge(Tea.for_user(user)) }
end
