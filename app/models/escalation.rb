# frozen_string_literal: true

class Escalation < ApplicationRecord
  belongs_to :escalation_profile

  validates :temperature, inclusion: 1..100, allow_nil: true
  validates :duration, numericality: { only_integer: true, greater_than: 0 }, allow_nil: true
  validates :rank, numericality: { only_integer: true, greater_than: 0 }

  validates_uniqueness_of :rank, scope: :escalation_profile

  scope :for_user, ->(user) { joins(escalation_profile: :tea).merge(Tea.for_user(user)) }
end
