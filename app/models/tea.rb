# frozen_string_literal: true

class Tea < ApplicationRecord
  has_many :servings
  has_many :steepings, through: :servings, dependent: :destroy
  has_many :escalation_profiles, autosave: true, index_errors: true, dependent: :destroy

  belongs_to :household

  validates :name, length: { maximum: 255 }, presence: true, uniqueness: { scope: :household_id }
  validates :description, length: { maximum: 2000 }, allow_blank: true
  validates :servings_remaining, numericality: { only_integer: true, greater_than_or_equal_to: 0 }, allow_nil: true
  validates :decommissioned, inclusion: { in: [true, false] }

  scope :for_user, ->(user) { where(household_id: user.household_id) }

  def escalation_profile(vessel)
    escalation_profiles.find_by(vessel: vessel) || escalation_profiles.first
  end

  def use_serving!
    return unless servings_remaining.present?
    return unless servings_remaining.positive?

    update!(servings_remaining: servings_remaining - 1)
  end

  def decommission!
    update!(decommissioned: true)
  end
end
