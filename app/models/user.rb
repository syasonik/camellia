# frozen_string_literal: true

class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable,
         :registerable,
         :recoverable,
         :rememberable,
         :validatable

  include GraphqlDevise::Concerns::Model

  belongs_to :household

  has_many :teas, through: :household
  has_many :vessels, through: :household

  before_validation :ensure_household, on: :create

  private

  def ensure_household
    return if household_id

    self.household = Household.new
  end
end
