# frozen_string_literal: true

class Vessel < ApplicationRecord
  belongs_to :household
  belongs_to :serving, optional: true

  has_many :escalation_profiles

  validates :name, length: { maximum: 255 }, presence: true, uniqueness: { scope: :household_id }
  validates :vessel_type, presence: true
  validates :decommissioned, inclusion: { in: [true, false] }

  enum vessel_type: {
    standard: 0,
    niche: 1
  }

  scope :for_user, ->(user) { where(household_id: user.household_id) }

  def set_serving!(serving)
    update!(serving: serving)
  end

  def empty!
    update!(serving_id: nil)
  end

  def decommission!
    update!(decommissioned: true)
  end
end
