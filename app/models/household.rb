# frozen_string_literal: true

class Household < ActiveRecord::Base
  has_many :teas, dependent: :delete_all
  has_many :vessels, dependent: :delete_all
  has_many :users, dependent: :delete_all

  def merge_household!(other_household)
    return if self == other_household

    users << other_household.users
    vessels << other_household.vessels
    teas << other_household.teas

    other_household.destroy!
  end
end
