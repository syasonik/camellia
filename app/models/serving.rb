# frozen_string_literal: true

class Serving < ApplicationRecord
  belongs_to :tea
  belongs_to :vessel
  has_many :steepings

  validates :is_finished, inclusion: { in: [true, false] }
  validates :vessel, uniqueness: { scope: :is_finished, message: 'should only have one unfinished serving' },
                     if: :in_progress?

  scope :for_user, ->(user) { joins(:tea).merge(Tea.for_user(user)) }

  DEFAULT_STEEP_DURATION = 4.minutes
  DEFAULT_STEEP_TEMPERATURE = 100

  def recommended_steep_params
    next_steep = next_escalation || last_steep

    {
      duration: next_steep&.duration || DEFAULT_STEEP_DURATION,
      temperature: next_steep&.temperature || DEFAULT_STEEP_TEMPERATURE,
      steep_number: next_steep_count
    }
  end

  def in_progress?
    !is_finished
  end

  def trash!
    vessel.empty!

    update!(is_finished: true)
  end

  def maximum_steep_number
    escalation_profile&.max_steeps
  end

  private

  def next_escalation
    escalation_profile&.escalations&.find_by(rank: next_steep_count)
  end

  def escalation_profile
    tea.escalation_profile(vessel)
  end

  def last_steep
    steepings.order(created_at: :desc).limit(1).first
  end

  def next_steep_count
    steep_count + 1
  end

  def steep_count
    steepings.count
  end
end
