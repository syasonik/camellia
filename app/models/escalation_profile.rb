# frozen_string_literal: true

class EscalationProfile < ApplicationRecord
  belongs_to :tea
  belongs_to :vessel, optional: true
  has_many :escalations, autosave: true, index_errors: true, dependent: :destroy
  has_many :ordered_escalations, -> { order(rank: :asc) }, class_name: 'Escalation'

  validates_uniqueness_of :tea, scope: :vessel

  scope :for_user, ->(user) { joins(:tea).merge(Tea.for_user(user)) }

  def max_steeps
    escalations.length
  end
end
