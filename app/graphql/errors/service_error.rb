# frozen_string_literal: true

module Errors
  class ServiceError < GraphQL::ExecutionError
    def initialize(message, type)
      @message = message
      @code = type.to_s.upcase.concat('_ERROR')

      super(@message)
    end

    def to_h
      super.merge(extensions: { code: @code })
    end
  end
end
