# frozen_string_literal: true

module Mutations
  class BaseMutation < GraphQL::Schema::Mutation
    def service_response(result)
      return result if result[:status] == :success

      raise Errors::ServiceError.new(result[:message], result[:type])
    end

    def current_user
      context[:current_resource]
    end
  end
end
