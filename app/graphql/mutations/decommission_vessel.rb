# frozen_string_literal: true

module Mutations
  class DecommissionVessel < Mutations::BaseMutation
    argument :vessel_id, ID, required: true

    field :vessel, Types::VesselType, null: true, description: 'Returns vessel if successfully decommissioned'

    def resolve(options)
      service_response Vessels::DecommissionService.new(current_user, options).execute
    end
  end
end
