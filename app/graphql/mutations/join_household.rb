# frozen_string_literal: true

module Mutations
  class JoinHousehold < Mutations::BaseMutation
    argument :household_id, ID, required: true

    field :household, Types::HouseholdType, null: true, description: 'Household the user is joining'

    def resolve(options)
      service_response Households::MergeService.new(current_user, options).execute
    end
  end
end
