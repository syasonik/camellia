# frozen_string_literal: true

module Mutations
  class CreateEscalationProfile < Mutations::BaseMutation
    argument :tea_id, ID, required: true
    argument :vessel_id, ID, required: false
    argument :escalations, [Types::EscalationInputType], required: false, description: 'Expected conditions for each steep of a tea'

    field :escalation_profile, Types::EscalationProfileType, null: true, description: 'Returns escalation_profile if successfully created'

    def resolve(options)
      service_response EscalationProfiles::CreateService.new(current_user, params_for(options)).execute
    end

    private

    def params_for(options)
      options
        .merge(escalations: options[:escalations]&.map(&:to_h))
        .compact
    end
  end
end
