# frozen_string_literal: true

module Mutations
  class EditVessel < Mutations::BaseMutation
    argument :vessel_id, ID, required: true
    argument :name, String, required: false
    argument :vessel_type, Types::VesselTypeEnum, required: false
    argument :decommissioned, Boolean, required: false

    field :vessel, Types::VesselType, null: true, description: 'Returns vessel if successfully updated'

    def resolve(options)
      service_response Vessels::EditService.new(current_user, options).execute
    end
  end
end
