# frozen_string_literal: true

module Mutations
  class DecommissionTea < Mutations::BaseMutation
    argument :tea_id, ID, required: true

    field :tea, Types::TeaType, null: true, description: 'Returns tea if successfully decommissioned'

    def resolve(options)
      service_response Teas::DecommissionService.new(current_user, options).execute
    end
  end
end
