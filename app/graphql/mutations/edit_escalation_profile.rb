# frozen_string_literal: true

module Mutations
  class EditEscalationProfile < Mutations::BaseMutation
    argument :escalation_profile_id, ID, required: false, description: 'ID for an existing escalation profile'
    argument :tea_id, ID, required: false, description: 'Tea id associated with the profile'
    argument :vessel_id, ID, required: false, description: 'Vessel id associated with the profile'
    argument :escalations, [Types::EscalationInputType], required: false, description: 'Expected conditions for each steep of a tea'

    field :escalation_profile, Types::EscalationProfileType, null: true, description: 'Returns profile if successfully updated'

    def resolve(options)
      service_response EscalationProfiles::EditService.new(current_user, params_for(options)).execute
    end

    private

    def params_for(options)
      options
        .except(:escalations)
        .merge(escalations: options[:escalations]&.map(&:to_h))
        .compact
    end
  end
end
