# frozen_string_literal: true

module Mutations
  class CreateServing < Mutations::BaseMutation
    argument :tea_id, ID, required: true
    argument :vessel_id, ID, required: true
    argument :steeping, Types::SteepingInputType, required: false

    field :serving, Types::ServingType, null: true, description: 'Returns serving if successfully created'

    def resolve(options)
      Serving.transaction do
        service_response serving_result = Servings::CreateService.new(
          current_user,
          options.except(:steeping)
        ).execute

        if options[:steeping]
          service_response Steepings::CreateService.new(
            current_user,
            **options[:steeping],
            serving_id: serving_result[:serving].id
          ).execute
        end

        serving_result
      end
    end
  end
end
