# frozen_string_literal: true

module Mutations
  class RateSteeping < Mutations::BaseMutation
    argument :steeping_id, ID, required: true
    argument :is_good, Boolean, required: false

    field :steeping, Types::SteepingType, null: true, description: 'Returns steeping if successfully rated'

    def resolve(options)
      service_response Steepings::EditService.new(current_user, is_good: nil, **options).execute
    end
  end
end
