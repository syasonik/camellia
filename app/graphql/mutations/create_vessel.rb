# frozen_string_literal: true

module Mutations
  class CreateVessel < Mutations::BaseMutation
    argument :name, String, required: true
    argument :vessel_type, Types::VesselTypeEnum, required: false, default_value: 'standard'

    field :vessel, Types::VesselType, null: true, description: 'Returns vessel if successfully created'

    def resolve(options)
      service_response Vessels::CreateService.new(current_user, options).execute
    end
  end
end
