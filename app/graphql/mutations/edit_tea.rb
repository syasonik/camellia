# frozen_string_literal: true

module Mutations
  class EditTea < Mutations::BaseMutation
    argument :tea_id, ID, required: true
    argument :name, String, required: false
    argument :description, String, required: false
    argument :decommissioned, Boolean, required: false
    argument :escalation_profiles, [Types::EscalationProfileInputType], required: false

    field :tea, Types::TeaType, null: true, description: 'Returns tea if successfully updated'

    def resolve(options)
      service_response Teas::EditService.new(current_user, params_for(options)).execute
    end

    private

    def params_for(options)
      options
        .except(:escalation_profiles)
        .merge(escalation_profiles: options[:escalation_profiles]&.map(&:to_h))
        .compact
    end
  end
end
