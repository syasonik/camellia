# frozen_string_literal: true

module Mutations
  class TrashServing < Mutations::BaseMutation
    argument :serving_id, ID, required: true

    field :serving, Types::ServingType, null: true, description: 'Returns serving if successfully destroyed'

    def resolve(options)
      service_response Servings::TrashService.new(current_user, options).execute
    end
  end
end
