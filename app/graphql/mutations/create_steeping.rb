# frozen_string_literal: true

module Mutations
  class CreateSteeping < Mutations::BaseMutation
    argument :serving_id, ID, required: true
    argument :temperature, Integer, required: false
    argument :duration, Integer, required: false
    argument :is_good, Boolean, required: false

    field :steeping, Types::SteepingType, null: true, description: 'Returns steeping if successfully created'

    def resolve(options)
      service_response Steepings::CreateService.new(current_user, options).execute
    end
  end
end
