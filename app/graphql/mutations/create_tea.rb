# frozen_string_literal: true

module Mutations
  class CreateTea < Mutations::BaseMutation
    argument :name, String, required: true
    argument :description, String, required: false
    argument :escalation_profiles, [Types::EscalationProfileInputType], required: false

    field :tea, Types::TeaType, null: true, description: 'Returns tea if successfully created'

    def resolve(options)
      service_response Teas::CreateService.new(current_user, params_for(options)).execute
    end

    private

    def params_for(options)
      options
        .except(:escalation_profiles)
        .merge(escalation_profiles: options[:escalation_profiles]&.map(&:to_h))
        .compact
    end
  end
end
