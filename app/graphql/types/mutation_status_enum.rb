# frozen_string_literal: true

module Types
  class MutationStatusEnum < Types::BaseEnum
    value 'ERROR', value: :error, description: 'Indicates an error was encountered'
    value 'SUCCESS', value: :success, description: 'Indicates the operation was successful'
  end
end
