# frozen_string_literal: true

module Types
  class HouseholdType < Types::BaseObject
    field :id, ID, null: false, description: 'Unique ID for joined household'
  end
end
