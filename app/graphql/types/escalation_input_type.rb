# frozen_string_literal: true

module Types
  class EscalationInputType < BaseInputObject
    argument :rank, Integer, required: true, description: 'Steep for which escalation should be used'
    argument :duration, Integer, required: true, description: 'Length of the steep (s)'
    argument :temperature, Integer, required: true, description: 'Temperature at which to steep the tea (C)'
  end
end
