# frozen_string_literal: true

module Types
  class SteepingType < Types::BaseObject
    field :id, ID, null: false, description: 'Unique ID for this steep'
    field :serving, Types::ServingType, null: false, description: 'Serving of tea to which this steeping belongs'
    field :temperature, Integer, null: false, description: 'Temperature at which this steep occurred (degrees Celcius)'
    field :duration, Integer, null: false, description: 'Length of time at which this steep occurred'
    field :steep_number, Integer, null: false, description: 'Number of steeps that have occurred at this point (including this one)'
    field :is_good, Boolean, null: true, description: 'Indicator of steep quality - tasty (true), or yucky (false)'
    field :created_at, GraphQL::Types::ISO8601DateTime, null: false, description: 'Creation time of the tea'
  end
end
