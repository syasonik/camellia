# frozen_string_literal: true

module Types
  class TeaType < Types::BaseObject
    field :id, ID, null: false, description: 'Unique ID for tea'
    field :name, String, null: false, description: 'Name used to identify tea'
    field :description, String, null: true, description: 'Description of tea'
    field :decommissioned, Boolean, null: true, description: 'Whether the tea is available to be steeped'
    field :servings_remaining, Integer, null: true, description: 'Estimation of how many servings remain of the tea'
    field :servings, [Types::ServingType], null: true, description: 'Instances of this tea being used'
    field :escalation_profiles, [Types::EscalationProfileType], null: true, description: 'Instructions for resteeping a serving of this tea'
  end
end
