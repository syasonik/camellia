# frozen_string_literal: true

module Types
  class SteepingInputType < BaseInputObject
    argument :duration, Integer, required: false, description: 'Length of the steep (s)'
    argument :temperature, Integer, required: false, description: 'Temperature of the steep (C)'
  end
end
