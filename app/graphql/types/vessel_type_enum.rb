# frozen_string_literal: true

module Types
  class VesselTypeEnum < Types::BaseEnum
    value 'STANDARD', value: 'standard', description: 'A vessel you wish to use regularly'
    value 'NICHE', value: 'niche', description: 'A vessel meant for special occasion or requirements'
  end
end
