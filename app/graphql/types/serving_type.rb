# frozen_string_literal: true

module Types
  class ServingType < Types::BaseObject
    field :id, ID, null: false, description: 'Unique ID for this tea serving'
    field :is_finished, Boolean, null: false, description: 'Indicator of whether this serving has been finished and discarded'
    field :tea, Types::TeaType, null: false, description: 'Type of tea used in the serving'
    field :vessel, Types::VesselType, null: true, description: 'Strainer or pot used to house the tea serving'
    field :steepings, [Types::SteepingType], null: false, description: 'Steepings that this serving has undegone so far'
    field :maximum_steep_number, Integer, null: true, description: 'Recommended total steep count for the serving'
    field :expected_next_duration, Integer, null: false, description: 'Recommended duration for next steep of this serving (s)'
    field :expected_next_temperature, Integer, null: false, description: 'Recommended temperature for next steep of this serving (degrees Celcius)'
    field :expected_next_steep_number, Integer, null: false, description: 'Expected steep count for the next steep of the serving'

    def expected_next_duration
      object.recommended_steep_params[:duration]
    end

    def expected_next_temperature
      object.recommended_steep_params[:temperature]
    end

    def expected_next_steep_number
      object.recommended_steep_params[:steep_number]
    end
  end
end
