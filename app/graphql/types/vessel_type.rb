# frozen_string_literal: true

module Types
  class VesselType < Types::BaseObject
    field :id, ID, null: false, description: 'Unique ID for this steeping vessel or apparatus'
    field :name, String, null: false, description: 'Name used to identify this vessel'
    field :vessel_type, Types::VesselTypeEnum, null: true, description: 'Indicates type of apparatus a vessel is'
    field :serving, Types::ServingType, null: true, description: 'Tea serving currently in the vessel, if any'
    field :decommissioned, Boolean, null: true, description: 'Whether the vessel is available to be used'
    field :escalation_profiles, [Types::EscalationProfileType], null: true, description: 'Instructions for resteeping teas in this vessel'
  end
end
