# frozen_string_literal: true

module Types
  class QueryType < Types::BaseObject
    field :teas, [Types::TeaType], null: false, description: 'Returns all teas for the user. Filtered to available standard teas by default.' do
      argument :id, ID, required: false, description: 'Filter teas by id'
      argument :decommissioned, Boolean, required: false, description: 'Filter teas based on whether they are decomissioned. Set to null to include all.', default_value: false
    end

    field :vessels, [Types::VesselType], null: false, description: 'Returns all vessels for the user. Filtered to available standard vessels by default.' do
      argument :id, ID, required: false, description: 'Filter vessels by id'
      argument :decommissioned, Boolean, required: false, description: 'Filter vessels based on whether they are decomissioned. Set to null to include all.', default_value: false
      argument :vessel_types, [Types::VesselTypeEnum], required: false, description: 'Filter vessels by type', default_value: ['standard']
    end

    def teas(**args)
      selection = Tea.for_user(current_user)
      selection = selection.where(decommissioned: args[:decommissioned]) unless args[:decommissioned].nil?
      selection = selection.where(id: args[:id]) unless args[:id].nil?

      selection
    end

    def vessels(**args)
      selection = Vessel.for_user(current_user)
      selection = selection.where(decommissioned: args[:decommissioned]) unless args[:decommissioned].nil?
      selection = selection.where(id: args[:id]) unless args[:id].nil?
      selection = selection.where(vessel_type: args[:vessel_types]) if args[:vessel_types].present?

      selection
    end
  end
end
