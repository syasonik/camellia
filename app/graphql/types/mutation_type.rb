# frozen_string_literal: true

module Types
  class MutationType < Types::BaseObject
    field :create_tea, mutation: Mutations::CreateTea
    field :decommission_tea, mutation: Mutations::DecommissionTea
    field :edit_tea, mutation: Mutations::EditTea

    field :create_vessel, mutation: Mutations::CreateVessel
    field :edit_vessel, mutation: Mutations::EditVessel
    field :decommission_vessel, mutation: Mutations::DecommissionVessel

    field :create_serving, mutation: Mutations::CreateServing
    field :trash_serving, mutation: Mutations::TrashServing

    field :create_steeping, mutation: Mutations::CreateSteeping
    field :rate_steeping, mutation: Mutations::RateSteeping

    field :create_escalation_profile, mutation: Mutations::CreateEscalationProfile
    field :edit_escalation_profile, mutation: Mutations::EditEscalationProfile

    field :join_household, mutation: Mutations::JoinHousehold, description: 'Combines the household of the user with the household provided'
  end
end
