# frozen_string_literal: true

module Types
  class EscalationType < Types::BaseObject
    field :id, ID, null: false, description: 'Unique ID for escalation'
    field :rank, Integer, null: false, description: 'Steep for which escalation should be used'
    field :duration, Integer, null: true, description: 'Length of the steep (s)'
    field :temperature, Integer, null: true, description: 'Temperature at which to steep the tea (C)'
  end
end
