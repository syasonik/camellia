# frozen_string_literal: true

module Types
  class EscalationProfileInputType < BaseInputObject
    argument :escalation_profile_id, ID, required: false, description: 'ID for an existing escalation profile'
    argument :vessel_id, ID, required: false, description: 'Vessel id associated with the profile'
    argument :escalations, [Types::EscalationInputType], required: false, description: 'Expected conditions for each steep of a tea'
  end
end
