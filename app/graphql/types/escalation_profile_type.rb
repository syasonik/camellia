# frozen_string_literal: true

module Types
  class EscalationProfileType < Types::BaseObject
    field :id, ID, null: false, description: 'Unique ID for tea'
    field :tea, Types::TeaType, null: false, description: 'Tea associated with the profile'
    field :vessel, Types::VesselType, null: true, description: 'Vessel associated with the profile'
    field :escalations, [Types::EscalationType], null: true, description: 'Expected conditions for each steep of a tea', method: :ordered_escalations
  end
end
