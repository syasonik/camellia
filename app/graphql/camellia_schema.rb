# frozen_string_literal: true

class CamelliaSchema < GraphQL::Schema
  NOT_AUTHENTICATED_EXCEPT_SCHEMA = lambda do |field|
    raise GraphqlDevise::AuthenticationError, "#{field} field requires authentication" unless field == '__schema'
  end

  use GraphqlDevise::SchemaPlugin.new(
    query: Types::QueryType,
    mutation: Types::MutationType,
    resource_loaders: [
      GraphqlDevise::ResourceLoader.new(User, only: [:login, :logout, :register]),
    ],
    unauthenticated_proc: NOT_AUTHENTICATED_EXCEPT_SCHEMA
  )
  mutation Types::MutationType
  query Types::QueryType
end
