# frozen_string_literal: true

class ApplicationController < ActionController::Base
  include GraphqlDevise::Concerns::SetUserByToken

  def not_found
    respond_to do |format|
      format.any { head :not_found }
    end
  end
end
