# frozen_string_literal: true

class GraphqlController < ApplicationController
  include ::GraphqlDevise::Concerns::SetUserByToken

  skip_before_action :verify_authenticity_token
  # If accessing from outside this domain, nullify the session
  # This allows for outside API access while preventing CSRF attacks,
  # but you'll have to authenticate your user separately
  # protect_from_forgery with: :null_session

  def execute
    variables = ensure_hash(params[:variables])

    insert_auth_params!(params) if circumvent_auth?

    query = params[:query]
    operation_name = params[:operationName]
    context = gql_devise_context(User)

    result = CamelliaSchema.execute(
      query,
      variables: variables,
      context: context,
      operation_name: operation_name
    )

    circumvent_auth!(context[:current_resource]) if circumvent_auth?

    render json: result unless performed?
  rescue StandardError => error
    raise error unless Rails.env.development?

    handle_error_in_development error
  end

  private

  # Handle form data, JSON body, or a blank value
  def ensure_hash(ambiguous_param)
    case ambiguous_param
    when String
      if ambiguous_param.present?
        ensure_hash(JSON.parse(ambiguous_param))
      else
        {}
      end
    when Hash, ActionController::Parameters
      ambiguous_param
    when nil
      {}
    else
      raise ArgumentError, "Unexpected parameter: #{ambiguous_param}"
    end
  end

  def circumvent_auth?
    Rails.env.development? && ActiveModel::Type::Boolean.new.cast(ENV['API_AUTHENTICATED'])
  end

  def insert_auth_params!(params)
    user = User.first
    new_token = user.create_new_auth_token

    params[:uid] ||= new_token['uid']
    params[:'access-token'] ||= new_token['access-token']
    params[:client] ||= new_token['client']
  end

  def circumvent_auth!(user)
    user.update!(tokens: user.tokens.except(['client']))
  end

  def handle_error_in_development(error)
    logger.error error.message
    logger.error error.backtrace.join("\n")

    render json: { errors: [{ message: error.message, backtrace: error.backtrace }], data: {} }, status: 500
  end
end
