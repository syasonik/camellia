# Configuration from https://github.com/docker/awesome-compose/tree/master/official-documentation-samples/rails/

FROM ruby:2.7.5
RUN apt-get update -qq && apt-get install -y nodejs postgresql-client
RUN mkdir /camellia-docker
WORKDIR /camellia-docker
COPY Gemfile /camellia-docker/Gemfile
COPY Gemfile.lock /camellia-docker/Gemfile.lock
RUN bundle install
COPY . /camellia-docker

# Add a script to be executed every time the container starts.
COPY entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]
EXPOSE 3000

# Start the main process.
CMD ["bin/rails", "server", "-b", "0.0.0.0"]
