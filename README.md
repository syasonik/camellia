### Camellia Daily API

Source code for server of Camellia, an app for tracking
daily tea consumption, timers, and inventories.

## Production FE

https://camellia.netlify.app/

### Working with production server

Test graphql queries using:

https://camellia-be.herokuapp.com/graphiql

Queries from the FE application should hit:

https://camellia-be.herokuapp.com/graphql

### Deployment

Server is hosted on Heroku, and deployments managed through
Heroku's container registry.

```
heroku container:push web
heroku container:release web
```

https://devcenter.heroku.com/articles/container-registry-and-runtime#getting-started

Migrations are not run by default with deploy. These can be run with:

```
heroku run docker exec web bin/rails db:migrate
```

### Local development

<details>
<summary>----- Instructions for installing prerequisites ------</summary>

##### Install Docker

https://docs.docker.com/get-docker/

##### Pull the repo

`git clone git@gitlab.com:syasonik/camellia.git`

##### Configure `.env` file

Duplicate `.env.example` as `.env` and adjust any environment variables as needed.

##### Setup the project
```
cd camellia
docker compose build
docker compose run web rake db:setup
docker compose up
```
</details>

#### Run the server
```
docker-compose up
```

Visit the server at `localhost:3000`. You can also [access the API locally](#accessing-the-api-locally).

#### Stop the server
```
CTRL+C
```

#### Apply docker config changes
Stop the server and rebuild, or run:

```
docker compose up --build
```

#### Accessing the rails console

The rails console can be used to interact with the database
and the rails application.

```
docker compose exec web bin/rails console
```

If you made a change to the rails code and want to pull in the
updated files:
```
reload!
```

To exit the rails console:
```
exit
```

#### Accessing the API locally

Visit `localhost:3000/graphiql` to experiment with the graphql API.

Send graphql POST requests to `localhost:3000/graphql`.

#### Resetting the database
If bad data has been saved to your local database or the
database has encountered problems, the DB can be wiped away
and reseeded.

```
docker compose exec web bin/rails db:reset
```
