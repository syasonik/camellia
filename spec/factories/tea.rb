# frozen_string_literal: true

FactoryBot.define do
  factory :tea do
    household
    name { Faker::Name.unique.name }
    description { Faker::Lorem.paragraph }
    decommissioned { false }

    trait :decommissioned do
      decommissioned { true }
    end

    trait :with_escalation do
      after(:create) do |tea, _|
        create(:escalation_profile, tea: tea)

        tea.reload
      end
    end

    trait :with_vessel_escalation do
      after(:create) do |tea, _evaluator|
        create(:escalation_profile, :with_vessel, tea: tea)

        tea.reload
      end
    end
  end
end
