# frozen_string_literal: true

FactoryBot.define do
  factory :escalation do
    temperature { Faker::Number.within(range: 1..100) }
    duration { Faker::Number.non_zero_digit }
  end
end
