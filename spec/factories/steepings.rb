# frozen_string_literal: true

FactoryBot.define do
  factory :steeping do
    serving
    temperature { Faker::Number.within(range: 1..100) }
    duration { Faker::Number.non_zero_digit }
    steep_number { 1 }
  end
end
