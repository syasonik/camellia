# frozen_string_literal: true

FactoryBot.define do
  factory :escalation_profile do
    tea

    after(:create) do |escalation_profile, _|
      3.times do |idx|
        create(:escalation, rank: idx + 1, escalation_profile: escalation_profile)
      end

      escalation_profile.reload
    end

    trait :with_vessel do
      vessel { create(:vessel, household: tea.household) }
    end
  end
end
