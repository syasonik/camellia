# frozen_string_literal: true

FactoryBot.define do
  factory :vessel do
    household
    name { Faker::Name.unique.name }
    vessel_type { :standard }
    decommissioned { false }

    trait :decommissioned do
      decommissioned { true }
    end

    trait :with_serving do
      after(:create) do |vessel, _|
        serving = create(:serving, vessel: vessel)
        vessel.update(serving: serving)
      end
    end
  end
end
