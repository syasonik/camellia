# frozen_string_literal: true

FactoryBot.define do
  factory :serving do
    tea
    vessel
    is_finished { false }

    trait :is_finished do
      is_finished { true }
    end
  end
end
