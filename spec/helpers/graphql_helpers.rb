# frozen_string_literal: true

module GraphqlHelpers
  def graphql_data
    response.parsed_body['data']
  end

  def graphql_errors
    response.parsed_body['errors']
  end

  def post_authenticated_graphql(variables = {}, headers = user.create_new_auth_token)
    post graphql_path, params: { query: query, variables: variables }, headers: headers, as: :json
  end

  def post_unauthenticated_graphql(variables = {})
    post graphql_path, params: { query: query, variables: variables }, as: :json
  end

  def mutation_input(fields)
    fields.present? ? "(#{fields.map { |field| "#{field}: $#{field}" }.join(', ')})" : ''
  end

  def mutation_input_format(fields)
    fields.present? ? "(#{fields.map { |field, type| "$#{field}: #{type}" }.join(', ')})" : ''
  end
end
