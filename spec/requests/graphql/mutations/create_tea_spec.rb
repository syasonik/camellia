# frozen_string_literal: true

require 'spec_helper'

RSpec.describe 'Create tea with escalation profile mutation' do
  include GraphqlHelpers

  let!(:user) { create(:user) }
  let(:db_tea) { Tea.last! }
  let(:db_profile) { db_tea.escalation_profiles.first }
  let(:db_profile2) { db_tea.escalation_profiles.second }

  let(:name) { 'New Tea' }
  let(:description) { 'I like this tea a lot' }
  let(:escalation_profiles) { [profile] }
  let(:profile) { { escalations: [escalation1, escalation2] } }
  let(:escalation1) { { rank: 1, duration: 60, temperature: 95 } }
  let(:escalation2) { { rank: 2, duration: 90, temperature: 95 } }

  let(:fields) do
    {
      name: 'String!',
      description: 'String',
      escalationProfiles: '[EscalationProfileInput!]'
    }.slice(*variables.keys)
  end
  let(:variables) do
    {
      name: name,
      description: description,
      escalationProfiles: escalation_profiles
    }.compact
  end
  let(:query) do
    <<-GRAPHQL
    mutation#{mutation_input_format(fields)} {
      createTea#{mutation_input(variables.keys)} {
        tea {
          id
          name
          description
          escalationProfiles {
            id
            vessel { id }
            tea { id }
            escalations {
              id
              rank
              duration
              temperature
            }
          }
        }
      }
    }
    GRAPHQL
  end

  let(:do_request) { post_authenticated_graphql(variables) }
  let(:tea) { response.parsed_body['data']['createTea']['tea'] }
  let(:errors) { response.parsed_body['errors'].to_s }
  let(:expected_tea) do
    {
      'id' => db_tea.id.to_s,
      'name' => 'New Tea',
      'description' => 'I like this tea a lot',
      'escalationProfiles' => expected_profiles
    }
  end
  let(:expected_profiles) do
    [
      {
        'id' => db_profile.id.to_s,
        'tea' => { 'id' => db_tea.id.to_s },
        'vessel' => nil,
        'escalations' => expected_escalations
      },
    ]
  end
  let(:expected_escalations) do
    [
      { 'duration' => 60, 'id' => db_profile.escalations.first.id.to_s, 'rank' => 1, 'temperature' => 95 },
      { 'duration' => 90, 'id' => db_profile.escalations.second.id.to_s, 'rank' => 2, 'temperature' => 95 },
    ]
  end

  before do
    do_request
  end

  shared_examples_for 'creates the tea' do
    it 'successfully' do
      expect(response).to have_http_status(:ok)
      expect(errors).to be_empty
      expect(tea).to eq(expected_tea)
    end
  end

  shared_examples_for 'returns an error' do |message|
    it "with message: #{message.truncate(15)}" do
      expect(response).to have_http_status(:ok)
      expect(graphql_errors.to_s).to include(message)
    end
  end

  it_behaves_like 'creates the tea'

  context 'for vessel escalation profile' do
    let(:vessel) { create(:vessel, household: user.household) }
    let(:profile) { { vesselId: vessel.id, escalations: [escalation1, escalation2] } }
    let(:expected_profiles) do
      [
        {
          'id' => db_profile.id.to_s,
          'tea' => { 'id' => db_tea.id.to_s },
          'vessel' => { 'id' => vessel.id.to_s },
          'escalations' => expected_escalations
        },
      ]
    end

    it_behaves_like 'creates the tea'

    context 'with invalid vesselId parameter' do
      let(:vessel) { create(:vessel) }

      it_behaves_like 'returns an error', "Couldn't find Vessel with 'id'"
    end
  end

  context 'for multiple escaltion_profiles' do
    let(:escalation_profiles) { [profile, profile] }

    before do
      expected_profiles << {
        'id' => db_profile2.id.to_s,
        'tea' => { 'id' => db_tea.id.to_s },
        'vessel' => nil,
        'escalations' => [
          { 'duration' => 60, 'id' => db_profile2.escalations.first.id.to_s, 'rank' => 1, 'temperature' => 95 },
          { 'duration' => 90, 'id' => db_profile2.escalations.second.id.to_s, 'rank' => 2, 'temperature' => 95 },
        ]
      }
    end

    it_behaves_like 'creates the tea'
  end

  context 'without required name parameter' do
    let(:name) {}

    it_behaves_like 'returns an error', 'missing required arguments: name'
  end

  context 'without optional description parameter' do
    let(:description) {}
    let(:expected_tea) do
      {
        'id' => db_tea.id.to_s,
        'name' => 'New Tea',
        'description' => nil,
        'escalationProfiles' => expected_profiles
      }
    end

    it_behaves_like 'creates the tea'
  end

  context 'with null optional escalationProfiles parameter' do
    let(:escalation_profiles) {}
    let(:expected_profiles) { [] }

    it_behaves_like 'creates the tea'
  end

  context 'without optional escalationProfiles elements' do
    let(:profile) {}
    let(:expected_profiles) { [] }

    it_behaves_like 'creates the tea'
  end

  context 'without required escalations parameter' do
    let(:profile) { [{}] }

    it_behaves_like 'returns an error', '[EscalationProfileInput!] was provided invalid value'
  end

  context 'without optional escalations elements' do
    let(:profile) { { escalations: [] } }
    let(:expected_escalations) { [] }

    it_behaves_like 'creates the tea'
  end

  context 'with null optional escalations parameter' do
    let(:profile) { { escalations: nil } }
    let(:expected_escalations) { [] }

    it_behaves_like 'creates the tea'
  end

  context 'without required escalation rank parameter' do
    let(:escalation1) { { duration: 60, temperature: 95 } }

    it_behaves_like 'returns an error', '[EscalationProfileInput!] was provided invalid value'
  end

  context 'without required escalation duration parameter' do
    let(:escalation1) { { rank: 1, temperature: 95 } }

    it_behaves_like 'returns an error', '[EscalationProfileInput!] was provided invalid value'
  end

  context 'without required escalation temperature paramter' do
    let(:escalation1) { { rank: 1, duration: 60 } }

    it_behaves_like 'returns an error', '[EscalationProfileInput!] was provided invalid value'
  end

  context 'with invalid tea parameter' do
    let(:name) { 'Name' * 200 }

    it_behaves_like 'returns an error', 'Validation failed: Name'
  end

  context 'with invalid escalation value' do
    let(:escalation1) { { rank: 1, duration: 60, temperature: 195 } }

    it_behaves_like 'returns an error', 'Validation failed: Escalation profiles[0] escalations[0] temperature'
  end

  context 'without logged-in user' do
    let(:do_request) { post_unauthenticated_graphql(variables) }

    it_behaves_like 'returns an error', 'field requires authentication'
  end
end
