# frozen_string_literal: true

require 'spec_helper'

RSpec.describe 'Rate steeping mutation' do
  include GraphqlHelpers

  let_it_be(:user) { create(:user) }
  let(:tea) { create(:tea, household: user.household) }
  let(:db_steeping) { create(:steeping, tea: tea) }

  let(:steeping_id) { db_steeping.id.to_s }
  let(:is_good) { true }

  let(:fields) do
    {
      steepingId: 'ID!',
      isGood: 'Boolean'
    }.slice(*variables.keys)
  end
  let(:variables) do
    {
      steepingId: steeping_id,
      isGood: is_good
    }.compact
  end
  let(:query) do
    <<-GRAPHQL
    mutation#{mutation_input_format(fields)} {
      rateSteeping#{mutation_input(variables.keys)} {
        steeping {
          id
          isGood
        }
      }
    }
    GRAPHQL
  end

  let(:do_request) { post_authenticated_graphql(variables) }
  let(:steeping) { response.parsed_body['data']['rateSteeping']['steeping'] }
  let(:errors) { response.parsed_body['errors'].to_s }
  let(:expected_steeping) do
    {
      'id' => steeping_id,
      'isGood' => expected_is_good
    }
  end
  let(:expected_is_good) { is_good }

  before do
    do_request
  end

  shared_examples_for 'updates the steeping' do
    it 'successfully' do
      expect(response).to have_http_status(:ok)
      expect(errors).to be_empty
      expect(steeping).to eq(expected_steeping)
    end
  end

  shared_examples_for 'returns an error' do |message|
    it "with message: #{message.truncate(15)}" do
      expect(response).to have_http_status(:ok)
      expect(graphql_errors.to_s).to include(message)
    end
  end

  it_behaves_like 'updates the steeping'

  context 'without isGood parameter' do
    let(:is_good) {}
    let(:expected_is_good) { nil }

    it_behaves_like 'updates the steeping'
  end

  context 'with invalid rating' do
    let(:is_good) { 'TRUE' }

    it_behaves_like 'returns an error', 'type Boolean was provided invalid value'
  end

  context 'with invalid steepingId parameter' do
    let(:steeping_id) { create(:steeping).id.to_s }

    it_behaves_like 'returns an error', "Couldn't find Steeping with 'id'"
  end

  context 'without logged-in user' do
    let(:do_request) { post_unauthenticated_graphql(variables) }

    it_behaves_like 'returns an error', 'field requires authentication'
  end
end
