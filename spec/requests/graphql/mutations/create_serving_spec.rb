# frozen_string_literal: true

require 'spec_helper'

RSpec.describe 'Create serving with steeping mutation' do
  include GraphqlHelpers

  let_it_be(:user) { create(:user) }
  let_it_be(:headers) { user.create_new_auth_token }
  let_it_be(:tea, reload: true) { create(:tea, :with_escalation, household: user.household) }
  let_it_be(:vessel, reload: true) { create(:vessel, household: user.household) }
  let(:next_escalation) { tea.escalation_profiles.first.escalations.first }

  let(:tea_id) { tea.id.to_s }
  let(:vessel_id) { vessel.id.to_s }
  let(:steeping) {}
  let(:db_serving) { Serving.last! }
  let(:db_steeping) { db_serving.steepings.first }

  let(:fields) do
    {
      teaId: 'ID!',
      vesselId: 'ID!',
      steeping: 'SteepingInput'
    }.slice(*variables.keys)
  end
  let(:variables) do
    {
      teaId: tea_id,
      vesselId: vessel_id,
      steeping: steeping
    }.compact
  end
  let(:query) do
    <<-GRAPHQL
    mutation#{mutation_input_format(fields)} {
      createServing#{mutation_input(variables.keys)} {
        serving {
          id
          isFinished
          maximumSteepNumber
          expectedNextDuration
          expectedNextTemperature
          expectedNextSteepNumber
          vessel {
            id
            serving { id }
          }
          tea { id }
          steepings {
            id
            serving { id }
            duration
            temperature
            steepNumber
            isGood
            createdAt
          }
        }
      }
    }
    GRAPHQL
  end

  let(:do_request) { post_authenticated_graphql(variables, headers) }
  let(:serving) { graphql_data['createServing']['serving'] }
  let(:errors) { graphql_errors.to_s }
  let(:expected_serving) do
    {
      'id' => db_serving.id.to_s,
      'isFinished' => false,
      'maximumSteepNumber' => expected_max_steep_number,
      'tea' => { 'id' => tea_id },
      'vessel' => { 'id' => vessel_id, 'serving' => { 'id' => db_serving.id.to_s } },
      'steepings' => expected_steepings
    }.merge(expected_next_params)
  end
  let(:expected_max_steep_number) { tea.escalation_profiles.first.escalations.length }
  let(:expected_next_params) do
    {
      'expectedNextDuration' => next_escalation.duration,
      'expectedNextSteepNumber' => next_escalation.rank,
      'expectedNextTemperature' => next_escalation.temperature
    }
  end
  let(:expected_steepings) { [] }
  let(:expected_steeping) do
    {
      'id' => db_steeping.id.to_s,
      'duration' => db_steeping.duration,
      'temperature' => db_steeping.temperature,
      'isGood' => nil,
      'steepNumber' => next_escalation.rank.to_i - 1,
      'createdAt' => db_steeping.created_at.iso8601,
      'serving' => { 'id' => db_serving.id.to_s }
    }
  end

  before do
    do_request
  end

  shared_examples_for 'creates the serving' do
    it 'successfully' do
      expect(response).to have_http_status(:ok)
      expect(errors).to be_empty
      expect(serving).to eq(expected_serving)
    end
  end

  shared_examples_for 'returns an error' do |message|
    it "with message: #{message.truncate(15)}" do
      expect(response).to have_http_status(:ok)
      expect(graphql_errors.to_s).to include(message)
    end

    it 'does not create a new serving' do
      expect { response }.not_to change(Serving, :count)
    end
  end

  it_behaves_like 'creates the serving'

  context 'with steeping parameter' do
    let(:next_escalation) { tea.escalation_profiles.first.escalations.second }
    let(:expected_steepings) { [expected_steeping] }

    context 'as empty object' do
      let(:steeping) { {} }

      it_behaves_like 'creates the serving'
    end

    context 'with duration paramter' do
      let(:steeping) { { duration: 600 } }
      let(:expected_steepings) { [expected_steeping.merge('duration' => 600)] }

      it_behaves_like 'creates the serving'
    end

    context 'with temperature parameter' do
      let(:steeping) { { temperature: 1 } }
      let(:expected_steepings) { [expected_steeping.merge('temperature' => 1)] }

      it_behaves_like 'creates the serving'
    end

    context 'with invalid steeping value' do
      let(:steeping) { { temperature: -1 } }

      it_behaves_like 'returns an error', 'Validation failed: Temperature'
    end
  end

  context 'when tea does not have escalation profile' do
    let_it_be(:tea, reload: true) { create(:tea, household: user.household) }
    let(:expected_max_steep_number) {}
    let(:next_escalation) do
      double(
        rank: 1,
        duration: Serving::DEFAULT_STEEP_DURATION,
        temperature: Serving::DEFAULT_STEEP_TEMPERATURE
      )
    end

    it_behaves_like 'creates the serving'
  end

  context 'without required teaId parameter' do
    let(:tea_id) {}

    it_behaves_like 'returns an error', 'missing required arguments: teaId'
  end

  context 'without required vesselId parameter' do
    let(:vessel_id) {}

    it_behaves_like 'returns an error', 'missing required arguments: vesselId'
  end

  context 'with invalid teaId parameter' do
    let_it_be(:tea_id) { create(:tea).id.to_s }

    it_behaves_like 'returns an error', "Couldn't find Tea with 'id'"
  end

  context 'with invalid vesselId parameter' do
    let_it_be(:vessel_id) { create(:vessel).id.to_s }

    it_behaves_like 'returns an error', "Couldn't find Vessel with 'id'"
  end

  context 'when vessel already has a serving' do
    let_it_be(:existing_serving) { create(:serving, tea: tea, vessel: vessel) }

    it_behaves_like 'returns an error', 'Validation failed: Vessel should only have one unfinished serving'
  end

  context 'without logged-in user' do
    let(:do_request) { post_unauthenticated_graphql(variables) }

    it_behaves_like 'returns an error', 'field requires authentication'
  end
end
