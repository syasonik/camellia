# frozen_string_literal: true

require 'spec_helper'

RSpec.describe 'Join household mutation' do
  include GraphqlHelpers

  let_it_be(:user, reload: true) { create(:user) }
  let_it_be(:original_tea) { create(:tea, household: user.household) }
  let_it_be(:original_vessel) { create(:vessel, household: user.household) }

  let_it_be(:other_user, reload: true) { create(:user) }
  let_it_be(:other_tea) { create(:tea, household: other_user.household) }
  let_it_be(:other_vessel) { create(:vessel, household: other_user.household) }

  let_it_be(:db_household) { other_user.household }

  let(:household_id) { db_household.id.to_s }

  let(:fields) do
    {
      householdId: 'ID!'
    }.slice(*variables.keys)
  end
  let(:variables) do
    {
      householdId: household_id
    }.compact
  end
  let(:query) do
    <<-GRAPHQL
    mutation#{mutation_input_format(fields)} {
      joinHousehold#{mutation_input(variables.keys)} {
        household {
          id
        }
      }
    }
    GRAPHQL
  end

  let(:do_request) { post_authenticated_graphql(variables) }
  let(:household) { response.parsed_body['data']['joinHousehold']['household'] }
  let(:errors) { response.parsed_body['errors'].to_s }
  let(:expected_household) do
    {
      'id' => household_id
    }
  end
  let(:expected_teas) { [original_tea, other_tea] }
  let(:expected_vessels) { [original_vessel, other_vessel] }

  shared_examples_for 'updates the household' do
    it 'successfully' do
      do_request

      expect(response).to have_http_status(:ok)
      expect(errors).to be_empty
      expect(household).to eq(expected_household)
      expect(db_household.reload.teas).to contain_exactly(*expected_teas)
      expect(db_household.vessels).to contain_exactly(*expected_vessels)
    end
  end

  shared_examples_for 'returns an error' do |message|
    it "with message: #{message.truncate(15)}" do
      do_request

      expect(response).to have_http_status(:ok)
      expect(graphql_errors.to_s).to include(message)
    end
  end

  it_behaves_like 'updates the household'

  context 'with household parameter for current household' do
    let(:db_household) { user.household }
    let(:expected_teas) { [original_tea] }
    let(:expected_vessels) { [original_vessel] }

    it_behaves_like 'updates the household'
  end

  context 'without required household parameter' do
    let(:household_id) {}

    it_behaves_like 'returns an error', 'missing required arguments: householdId'
  end

  context 'with invalid householdId parameter' do
    let(:household_id) { '-1' }

    it_behaves_like 'returns an error', "Couldn't find Household with 'id'"
  end

  context 'without logged-in user' do
    let(:do_request) { post_unauthenticated_graphql(variables) }

    it_behaves_like 'returns an error', 'field requires authentication'
  end
end
