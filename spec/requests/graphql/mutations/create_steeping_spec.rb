# frozen_string_literal: true

require 'spec_helper'

RSpec.describe 'Create steeping mutation' do
  include GraphqlHelpers

  let_it_be(:user) { create(:user) }
  let_it_be(:headers) { user.create_new_auth_token }
  let_it_be(:tea, reload: true) { create(:tea, :with_escalation, household: user.household) }
  let_it_be(:serving, reload: true) { create(:serving, tea: tea) }

  let(:serving_id) { serving.id.to_s }
  let(:temperature) { nil }
  let(:duration) { nil }
  let(:is_good) { nil }
  let(:db_steeping) { Steeping.last! }

  let(:fields) do
    {
      servingId: 'ID!',
      temperature: 'Int',
      duration: 'Int',
      isGood: 'Boolean'
    }.slice(*variables.keys)
  end
  let(:variables) do
    {
      servingId: serving_id,
      temperature: temperature,
      duration: duration,
      isGood: is_good
    }.compact
  end
  let(:query) do
    <<-GRAPHQL
    mutation#{mutation_input_format(fields)} {
      createSteeping#{mutation_input(variables.keys)} {
        steeping {
          id
          temperature
          duration
          steepNumber
          isGood
          serving { id }
        }
      }
    }
    GRAPHQL
  end

  let(:do_request) { post_authenticated_graphql(variables, headers) }
  let(:steeping) { graphql_data['createSteeping']['steeping'] }
  let(:errors) { graphql_errors.to_s }
  let(:expected_steeping) do
    {
      'id' => db_steeping.id.to_s,
      'duration' => duration || db_steeping.duration,
      'temperature' => temperature || db_steeping.temperature,
      'isGood' => is_good || db_steeping.is_good,
      'steepNumber' => expected_steeping_number,
      'serving' => { 'id' => serving.id.to_s }
    }
  end
  let(:expected_steeping_number) { 1 }

  before do
    do_request
  end

  shared_examples_for 'creates the steeping' do
    it 'successfully' do
      expect(response).to have_http_status(:ok)
      expect(errors).to be_empty
      expect(steeping).to eq(expected_steeping)
    end
  end

  shared_examples_for 'returns an error' do |message|
    it "with message: #{message.truncate(15)}" do
      expect(response).to have_http_status(:ok)
      expect(graphql_errors.to_s).to include(message)
    end

    it 'does not create a new steeping' do
      expect { response }.not_to change(Steeping, :count)
    end
  end

  it_behaves_like 'creates the steeping'

  context 'with only servingID' do
    it_behaves_like 'creates the steeping'

    context 'when serving already has a steeping' do
      let_it_be(:existing_steeping) { create(:steeping, serving: serving) }
      let(:expected_steeping_number) { 2 }

      it_behaves_like 'creates the steeping'
    end

    context 'with invalid servingId parameter' do
      let_it_be(:serving_id) { create(:serving).id.to_s }

      it_behaves_like 'returns an error', "Couldn't find Serving with 'id'"
    end
  end

  context 'with duration paramter' do
    let(:duration) { 600 }

    it_behaves_like 'creates the steeping'

    context 'with invalid duration' do
      let(:duration) { 0 }

      it_behaves_like 'returns an error', 'Validation failed: Duration'
    end
  end

  context 'with temperature parameter' do
    let(:temperature) { 1 }

    it_behaves_like 'creates the steeping'

    context 'with invalid temperature' do
      let(:temperature) { -1 }

      it_behaves_like 'returns an error', 'Validation failed: Temperature'
    end
  end

  context 'with rating parameter' do
    let(:is_good) { false }

    it_behaves_like 'creates the steeping'

    context 'with invalid rating' do
      let(:is_good) { 'TRUE' }

      it_behaves_like 'returns an error', 'type Boolean was provided invalid value'
    end
  end

  context 'without required servingId parameter' do
    let(:serving_id) {}

    it_behaves_like 'returns an error', 'missing required arguments: servingId'
  end

  context 'without logged-in user' do
    let(:do_request) { post_unauthenticated_graphql(variables) }

    it_behaves_like 'returns an error', 'field requires authentication'
  end
end
