# frozen_string_literal: true

require 'spec_helper'

RSpec.describe 'Edit tea mutation' do
  include GraphqlHelpers

  let_it_be(:user) { create(:user) }
  let_it_be(:headers) { user.create_new_auth_token }
  let_it_be(:db_tea) { create(:tea, :with_escalation, :with_vessel_escalation, household: user.household) }
  let_it_be(:db_default_profile) { db_tea.escalation_profiles.first }
  let_it_be(:db_vessel_profile) { db_tea.escalation_profiles.second }
  let_it_be(:db_vessel) { db_vessel_profile.vessel }
  let_it_be(:db_other_vessel) { create(:vessel, household: user.household) }
  let_it_be(:original_default_escalations) { escalations_attributes(db_default_profile.escalations) }
  let_it_be(:original_vessel_escalations) { escalations_attributes(db_vessel_profile.escalations) }

  let(:tea_id) { db_tea.id.to_s }
  let(:name) { 'New Tea' }
  let(:description) { 'I like this tea a lot' }
  let(:decommissioned) { false }
  let(:profiles) { [default_profile, vessel_profile] }
  let(:default_profile) { { escalationProfileId: db_default_profile.id.to_s, escalations: default_escalations } }
  let(:vessel_profile) do
    { escalationProfileId: db_vessel_profile.id.to_s,
      vesselId: db_vessel.id.to_s,
      escalations: vessel_escalations }
  end
  let(:default_escalations) { escalations_attributes(db_default_profile.escalations) }
  let(:vessel_escalations) { escalations_attributes(db_vessel_profile.escalations) }

  let(:fields) do
    {
      teaId: 'ID!',
      name: 'String',
      description: 'String',
      decommissioned: 'Boolean',
      escalationProfiles: '[EscalationProfileInput!]'
    }.slice(*variables.keys)
  end
  let(:variables) do
    {
      teaId: tea_id,
      name: name,
      description: description,
      decommissioned: decommissioned,
      escalationProfiles: profiles
    }.compact
  end
  let(:query) do
    <<-GRAPHQL
    mutation#{mutation_input_format(fields)} {
      editTea#{mutation_input(variables.keys)} {
        tea {
          id
          name
          description
          decommissioned
          escalationProfiles {
            id
            tea { id }
            vessel { id }
            escalations {
              rank
              temperature
              duration
            }
          }
        }
      }
    }
    GRAPHQL
  end

  let(:do_request) { post_authenticated_graphql(variables, headers) }
  let(:tea) { response.parsed_body['data']['editTea']['tea'] }
  let(:errors) { response.parsed_body['errors'].to_s }
  let(:expected_tea) do
    {
      'id' => tea_id,
      'name' => expected_name,
      'description' => expected_description,
      'decommissioned' => expected_decommissioned,
      'escalationProfiles' => expected_profiles
    }
  end
  let(:expected_name) { name }
  let(:expected_description) { description }
  let(:expected_decommissioned) { decommissioned }
  let(:expected_profiles) { [expected_default_profile, expected_vessel_profile] }
  let(:expected_default_profile) do
    {
      'id' => db_default_profile.id.to_s,
      'tea' => { 'id' => db_tea.id.to_s },
      'vessel' => nil,
      'escalations' => expected_default_escalations
    }
  end
  let(:expected_vessel_profile) do
    {
      'id' => expected_vessel_profile_id,
      'tea' => { 'id' => db_tea.id.to_s },
      'vessel' => { 'id' => db_vessel.id.to_s },
      'escalations' => expected_vessel_escalations
    }
  end
  let(:expected_default_escalations) { original_default_escalations }
  let(:expected_vessel_escalations) { original_vessel_escalations }
  let(:expected_vessel_profile_id) { db_vessel_profile.id.to_s }

  before do
    do_request
  end

  shared_examples_for 'updates the tea' do
    it 'successfully' do
      expect(response).to have_http_status(:ok)
      expect(errors).to be_empty
      expect(tea).to eq(expected_tea)
    end
  end

  shared_examples_for 'returns an error' do |message|
    let!(:original_tea_attributes) { all_tea_attributes(db_tea) }
    let(:new_tea_attributes) { all_tea_attributes(db_tea.reload) }

    it "with message: #{message.truncate(15)}" do
      expect(response).to have_http_status(:ok)
      expect(graphql_errors.to_s).to include(message)
      expect(new_tea_attributes).to eq(original_tea_attributes)
    end

    def all_tea_attributes(tea)
      tea.attributes.merge(profiles: tea.escalation_profiles.map { |profile| all_profile_attributes(profile) })
    end

    def all_profile_attributes(profile)
      profile.attributes.merge(escalations: profile.escalations.map(&:attributes))
    end
  end

  it_behaves_like 'updates the tea'

  context 'without name parameter' do
    let(:name) {}
    let(:expected_name) { db_tea.name }

    it_behaves_like 'updates the tea'
  end

  context 'without description parameter' do
    let(:description) {}
    let(:expected_description) { db_tea.description }

    it_behaves_like 'updates the tea'
  end

  context 'without decommissioned parameter' do
    let(:decommissioned) {}
    let(:expected_decommissioned) { false }

    it_behaves_like 'updates the tea'
  end

  context 'without escalation profile changes' do
    let(:escalation_profiles) {}

    it_behaves_like 'updates the tea'
  end

  context 'when decommissioning' do
    let(:decommissioned) { true }

    it_behaves_like 'updates the tea'
  end

  context 'when removing an escalation profile' do
    let(:profiles) { [vessel_profile] }
    let(:expected_profiles) { [expected_vessel_profile] }

    it_behaves_like 'updates the tea'

    context 'removing a vessel profile' do
      let(:profiles) { [default_profile] }
      let(:expected_profiles) { [expected_default_profile] }

      it_behaves_like 'updates the tea'
    end
  end

  context 'when removing all esclation profiles' do
    let(:profiles) { [] }
    let(:expected_profiles) { [] }

    it_behaves_like 'updates the tea'
  end

  context 'when adding a new escalation profile' do
    let(:expected_new_profile) do
      {
        'id' => EscalationProfile.last.id.to_s,
        'tea' => { 'id' => db_tea.id.to_s },
        'vessel' => expected_vessel_id,
        'escalations' => new_profile_escalations.map(&:stringify_keys)
      }
    end
    let(:new_profile_escalations) do
      [
        { rank: 1, duration: 60, temperature: 95 },
        { rank: 2, duration: 90, temperature: 95 },
      ]
    end

    context 'when adding a new default profile' do
      let(:profiles) { [vessel_profile, new_profile] }
      let(:new_profile) { { escalations: new_profile_escalations } }
      let(:expected_profiles) { [expected_vessel_profile, expected_new_profile] }
      let(:expected_vessel_id) {}

      it_behaves_like 'updates the tea'

      context 'with invalid escalation parameter' do
        let(:new_profile_escalations) { [{ rank: 1, duration: 60, temperature: 195 }] }

        it_behaves_like 'returns an error', 'Validation failed: Escalation profiles[1] escalations[0] temperature'
      end
    end

    context 'adding a vessel profile' do
      let(:profiles) { [default_profile, vessel_profile, new_profile] }
      let(:new_profile) { { escalations: new_profile_escalations, vesselId: db_other_vessel.id.to_s } }
      let(:expected_profiles) { [expected_default_profile, expected_vessel_profile, expected_new_profile] }
      let(:expected_vessel_id) { { 'id' => db_other_vessel.id.to_s } }

      it_behaves_like 'updates the tea'

      context 'with invalid escalation parameter' do
        let(:new_profile_escalations) { [{ rank: 1, duration: 60, temperature: 195 }] }

        it_behaves_like 'returns an error', 'Validation failed: Escalation profiles[2] escalations[0] temperature'
      end
    end

    context 'adding or replacing multiple escalation profiles' do
      let(:profiles) { [default_profile, vessel_profile, new_profile] }
      let(:new_profile) { { vesselId: db_other_vessel.id.to_s, escalations: new_profile_escalations } }
      let(:vessel_profile) { { vesselId: db_vessel.id.to_s, escalations: vessel_escalations } }
      let(:expected_profiles) { [expected_default_profile, expected_vessel_profile, expected_new_profile] }
      let(:expected_vessel_id) { { 'id' => db_other_vessel.id.to_s } }
      let(:expected_vessel_profile_id) { (EscalationProfile.last.id - 1).to_s }

      it_behaves_like 'updates the tea'
    end
  end

  context 'when updating an escalation profile' do
    context 'for a default profile' do
      let(:default_escalations) do
        [
          { rank: 1, duration: 60, temperature: 95 },
          { rank: 2, duration: 90, temperature: 95 },
        ]
      end
      let(:expected_default_escalations) { default_escalations.map(&:stringify_keys) }

      it_behaves_like 'updates the tea'
    end

    context 'updating a vessel profile' do
      let(:vessel_escalations) do
        [
          { rank: 1, duration: 60, temperature: 95 },
          { rank: 2, duration: 90, temperature: 95 },
        ]
      end
      let(:expected_vessel_escalations) { vessel_escalations.map(&:stringify_keys) }

      it_behaves_like 'updates the tea'
    end

    context 'adding an escalation' do
      let(:new_escalation) { { rank: 4, duration: 60, temperature: 95 }.stringify_keys }
      let(:default_escalations) do
        [
          *original_default_escalations[0..1],
          new_escalation,
          original_default_escalations[2],
        ]
      end
      let(:expected_default_escalations) { [*original_default_escalations, new_escalation] }

      it_behaves_like 'updates the tea'

      context 'with invalid escalation parameter' do
        let(:new_escalation) { { rank: 4, duration: 60, temperature: 195 }.stringify_keys }

        it_behaves_like 'returns an error', 'Validation failed: Escalation profiles[0] escalations[2] temperature'
      end
    end

    context 'removing an escalation' do
      let(:default_escalations) { original_default_escalations[0..-2] }
      let(:expected_default_escalations) { default_escalations }

      it_behaves_like 'updates the tea'
    end

    context 'removing all escalations' do
      let(:default_escalations) { [] }
      let(:expected_default_escalations) { default_escalations }

      it_behaves_like 'updates the tea'
    end

    context 'reordering escalations' do
      let(:default_escalations) do
        original_default_escalations.map { |escalation| escalation.merge('rank' => 4 - escalation[:rank]) }
      end
      let(:expected_default_escalations) { default_escalations.sort_by { |escalation| escalation['rank'] } }

      it_behaves_like 'updates the tea'
    end

    context 'with invalid vessel id' do
      let(:vessel_profile) { { vesselId: create(:vessel).id.to_s } }

      it_behaves_like 'returns an error', "Couldn't find Vessel with 'id'"
    end

    context 'with invalid profile id' do
      let(:default_profile) { { escalationProfileId: create(:escalation_profile).id.to_s } }

      it_behaves_like 'returns an error', "Couldn't find EscalationProfiles with 'id'"
    end

    context 'with invalid escalation parameter' do
      let(:default_escalations) { [{ rank: 1, duration: 60, temperature: 195 }.stringify_keys] }

      it_behaves_like 'returns an error', 'Validation failed: Escalation profiles[0] escalations[0] temperature'
    end

    context 'with multiple profiles for the same vessel' do
      let(:profiles) { [default_profile, vessel_profile, vessel_profile] }

      it_behaves_like 'returns an error', 'Teas may only have one escalation profile per vessel and a default profile'
    end

    context 'with multiple default profiles' do
      let(:profiles) { [default_profile, default_profile.except(:escalationProfileId)] }

      it_behaves_like 'returns an error', 'Teas may only have one escalation profile per vessel and a default profile'
    end

    context 'with multiple escalations with the same rank' do
      let(:new_escalation) { { rank: 3, duration: 60, temperature: 95 }.stringify_keys }
      let(:default_escalations) { [*original_default_escalations, new_escalation] }

      it_behaves_like 'returns an error', 'Escalation profiles may only have one escalation per rank'
    end
  end

  context 'with invalid name parameter' do
    let(:name) { 'Name' * 200 }

    it_behaves_like 'returns an error', 'Validation failed: Name'
  end

  context 'with invalid description parameter' do
    let(:description) { 'Description' * 2000 }

    it_behaves_like 'returns an error', 'Validation failed: Description'
  end

  context 'with invalid teaId parameter' do
    let(:tea_id) { create(:tea).id.to_s }

    it_behaves_like 'returns an error', "Couldn't find Tea with 'id'"
  end

  context 'without logged-in user' do
    let(:do_request) { post_unauthenticated_graphql(variables) }

    it_behaves_like 'returns an error', 'field requires authentication'
  end

  private

  def escalations_attributes(escalations)
    escalations.map do |escalation|
      escalation.slice(:rank, :temperature, :duration)
    end
  end
end
