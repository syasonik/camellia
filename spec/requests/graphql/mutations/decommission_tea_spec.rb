# frozen_string_literal: true

require 'spec_helper'

RSpec.describe 'Decomission tea mutation' do
  include GraphqlHelpers

  let_it_be(:user) { create(:user) }
  let_it_be(:db_tea, reload: true) { create(:tea, household: user.household) }

  let(:tea_id) { db_tea.id.to_s }

  let(:fields) do
    {
      teaId: 'ID!'
    }.slice(*variables.keys)
  end
  let(:variables) do
    {
      teaId: tea_id
    }.compact
  end
  let(:query) do
    <<-GRAPHQL
    mutation#{mutation_input_format(fields)} {
      decommissionTea#{mutation_input(variables.keys)} {
        tea {
          id
          decommissioned
        }
      }
    }
    GRAPHQL
  end

  let(:do_request) { post_authenticated_graphql(variables) }
  let(:tea) { response.parsed_body['data']['decommissionTea']['tea'] }
  let(:errors) { response.parsed_body['errors'].to_s }
  let(:expected_tea) do
    {
      'id' => tea_id,
      'decommissioned' => true
    }
  end

  before do
    do_request
  end

  shared_examples_for 'updates the tea' do
    it 'successfully' do
      expect(response).to have_http_status(:ok)
      expect(errors).to be_empty
      expect(tea).to eq(expected_tea)
    end
  end

  shared_examples_for 'returns an error' do |message|
    it "with message: #{message.truncate(15)}" do
      expect(response).to have_http_status(:ok)
      expect(graphql_errors.to_s).to include(message)
    end
  end

  it_behaves_like 'updates the tea'

  context 'for a decommissioned tea' do
    before do
      db_tea.update!(decommissioned: true)
    end

    it_behaves_like 'updates the tea'
  end

  context 'with invalid teaId parameter' do
    let(:tea_id) { create(:tea).id.to_s }

    it_behaves_like 'returns an error', "Couldn't find Tea with 'id'"
  end

  context 'without logged-in user' do
    let(:do_request) { post_unauthenticated_graphql(variables) }

    it_behaves_like 'returns an error', 'field requires authentication'
  end
end
