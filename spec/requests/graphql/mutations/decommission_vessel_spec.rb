# frozen_string_literal: true

require 'spec_helper'

RSpec.describe 'Decomission vessel mutation' do
  include GraphqlHelpers

  let_it_be(:user) { create(:user) }
  let_it_be(:db_vessel, reload: true) { create(:vessel, household: user.household) }

  let(:vessel_id) { db_vessel.id.to_s }

  let(:fields) do
    {
      vesselId: 'ID!'
    }.slice(*variables.keys)
  end
  let(:variables) do
    {
      vesselId: vessel_id
    }.compact
  end
  let(:query) do
    <<-GRAPHQL
    mutation#{mutation_input_format(fields)} {
      decommissionVessel#{mutation_input(variables.keys)} {
        vessel {
          id
          decommissioned
        }
      }
    }
    GRAPHQL
  end

  let(:do_request) { post_authenticated_graphql(variables) }
  let(:vessel) { response.parsed_body['data']['decommissionVessel']['vessel'] }
  let(:errors) { response.parsed_body['errors'].to_s }
  let(:expected_vessel) do
    {
      'id' => vessel_id,
      'decommissioned' => true
    }
  end

  before do
    do_request
  end

  shared_examples_for 'updates the vessel' do
    it 'successfully' do
      expect(response).to have_http_status(:ok)
      expect(errors).to be_empty
      expect(vessel).to eq(expected_vessel)
    end
  end

  shared_examples_for 'returns an error' do |message|
    it "with message: #{message.truncate(15)}" do
      expect(response).to have_http_status(:ok)
      expect(graphql_errors.to_s).to include(message)
    end
  end

  it_behaves_like 'updates the vessel'

  context 'for a decommissioned vessel' do
    before do
      db_vessel.update!(decommissioned: true)
    end

    it_behaves_like 'updates the vessel'
  end

  context 'with invalid vesselId parameter' do
    let(:vessel_id) { create(:vessel).id.to_s }

    it_behaves_like 'returns an error', "Couldn't find Vessel with 'id'"
  end

  context 'without logged-in user' do
    let(:do_request) { post_unauthenticated_graphql(variables) }

    it_behaves_like 'returns an error', 'field requires authentication'
  end
end
