# frozen_string_literal: true

require 'spec_helper'

RSpec.describe 'Create escalation profile mutation' do
  include GraphqlHelpers

  let_it_be(:user) { create(:user) }
  let_it_be(:headers) { user.create_new_auth_token }
  let_it_be(:tea, reload: true) { create(:tea, household: user.household) }
  let(:db_profile) { EscalationProfile.last! }

  let(:tea_id) { tea.id.to_s }
  let(:vessel_id) { nil }
  let(:escalations) { [escalation1, escalation2] }
  let(:escalation1) { { rank: 1, duration: 60, temperature: 95 } }
  let(:escalation2) { { rank: 2, duration: 90, temperature: 95 } }

  let(:fields) do
    {
      teaId: 'ID!',
      vesselId: 'ID',
      escalations: '[EscalationInput!]'
    }.slice(*variables.keys)
  end
  let(:variables) do
    {
      teaId: tea_id,
      vesselId: vessel_id,
      escalations: escalations
    }.compact
  end
  let(:query) do
    <<-GRAPHQL
    mutation#{mutation_input_format(fields)} {
      createEscalationProfile#{mutation_input(variables.keys)} {
        escalationProfile {
          id
          vessel { id }
          tea { id }
          escalations {
            id
            rank
            duration
            temperature
          }
        }
      }
    }
    GRAPHQL
  end

  let(:do_request) { post_authenticated_graphql(variables, headers) }
  let(:profile) { response.parsed_body['data']['createEscalationProfile']['escalationProfile'] }
  let(:errors) { response.parsed_body['errors'].to_s }
  let(:expected_profile) do
    {
      'id' => db_profile.id.to_s,
      'tea' => { 'id' => tea_id },
      'vessel' => nil,
      'escalations' => expected_escalations
    }
  end
  let(:expected_escalations) do
    [
      { 'duration' => 60, 'id' => db_profile.escalations.first.id.to_s, 'rank' => 1, 'temperature' => 95 },
      { 'duration' => 90, 'id' => db_profile.escalations.second.id.to_s, 'rank' => 2, 'temperature' => 95 },
    ]
  end

  before do
    do_request
  end

  shared_examples_for 'creates the profile' do
    it 'successfully' do
      expect(response).to have_http_status(:ok)
      expect(errors).to be_empty
      expect(profile).to eq(expected_profile)
    end
  end

  shared_examples_for 'returns an error' do |message|
    it "with message: #{message.truncate(15)}" do
      expect(response).to have_http_status(:ok)
      expect(graphql_errors.to_s).to include(message)
    end
  end

  it_behaves_like 'creates the profile'

  context 'for vessel escalation profile' do
    let_it_be(:vessel) { create(:vessel, household: user.household) }
    let(:vessel_id) { vessel.id.to_s }

    it_behaves_like 'creates the profile' do
      before do
        expected_profile['vessel'] = { 'id' => vessel_id }
      end
    end

    context 'with invalid vesselId parameter' do
      let_it_be(:vessel) { create(:vessel) }

      it_behaves_like 'returns an error', "Couldn't find Vessel with 'id'"
    end

    context 'with existing escalation profile for vessel' do
      let_it_be(:existing_profile) { create(:escalation_profile, tea: tea, vessel: vessel) }

      it_behaves_like 'returns an error', 'Validation failed: Tea has already been taken'
    end
  end

  context 'without required teaId parameter' do
    let(:tea_id) {}

    it_behaves_like 'returns an error', 'missing required arguments: teaId'
  end

  context 'with existing default escalation profile' do
    let_it_be(:existing_profile) { create(:escalation_profile, tea: tea) }

    it_behaves_like 'returns an error', 'Validation failed: Tea has already been taken'
  end

  context 'with invalid teaId parameter' do
    let_it_be(:tea_id) { create(:tea).id.to_s }

    it_behaves_like 'returns an error', "Couldn't find Tea with 'id'"
  end

  context 'without optional escalations parameter' do
    let(:expected_escalations) { [] }
    let(:escalations) {}

    it_behaves_like 'creates the profile'
  end

  context 'without optional escalations elements' do
    let(:expected_escalations) { [] }
    let(:escalations) { [] }

    it_behaves_like 'creates the profile'
  end

  context 'without required escalation rank parameter' do
    let(:escalation1) { { duration: 60, temperature: 95 } }

    it_behaves_like 'returns an error', '[EscalationInput!] was provided invalid value'
  end

  context 'without required escalation duration parameter' do
    let(:escalation1) { { rank: 1, temperature: 95 } }

    it_behaves_like 'returns an error', '[EscalationInput!] was provided invalid value'
  end

  context 'without required escalation temperature paramter' do
    let(:escalation1) { { rank: 1, duration: 60 } }

    it_behaves_like 'returns an error', '[EscalationInput!] was provided invalid value'
  end

  context 'with invalid escalation value' do
    let(:escalation1) { { rank: 1, duration: 60, temperature: 195 } }

    it_behaves_like 'returns an error', 'Validation failed: Escalations[0] temperature'
  end

  context 'without logged-in user' do
    let(:do_request) { post_unauthenticated_graphql(variables) }

    it_behaves_like 'returns an error', 'field requires authentication'
  end
end
