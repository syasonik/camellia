# frozen_string_literal: true

require 'spec_helper'

RSpec.describe 'Edit vessel mutation' do
  include GraphqlHelpers

  let_it_be(:user) { create(:user) }
  let(:db_vessel) { create(:vessel, household: user.household) }

  let(:vessel_id) { db_vessel.id.to_s }
  let(:name) { 'New Vessel' }
  let(:vessel_type) { 'NICHE' }
  let(:decommissioned) { false }

  let(:fields) do
    {
      vesselId: 'ID!',
      name: 'String',
      vesselType: 'VesselTypeEnum',
      decommissioned: 'Boolean'
    }.slice(*variables.keys)
  end
  let(:variables) do
    {
      vesselId: vessel_id,
      name: name,
      vesselType: vessel_type,
      decommissioned: decommissioned
    }.compact
  end
  let(:query) do
    <<-GRAPHQL
    mutation#{mutation_input_format(fields)} {
      editVessel#{mutation_input(variables.keys)} {
        vessel {
          id
          name
          vesselType
          decommissioned
        }
      }
    }
    GRAPHQL
  end

  let(:do_request) { post_authenticated_graphql(variables) }
  let(:vessel) { response.parsed_body['data']['editVessel']['vessel'] }
  let(:errors) { response.parsed_body['errors'].to_s }
  let(:expected_vessel) do
    {
      'id' => vessel_id,
      'name' => expected_name,
      'vesselType' => expected_vessel_type,
      'decommissioned' => expected_decommissioned
    }
  end
  let(:expected_name) { name }
  let(:expected_vessel_type) { vessel_type }
  let(:expected_decommissioned) { decommissioned }

  before do
    do_request
  end

  shared_examples_for 'updates the vessel' do
    it 'successfully' do
      expect(response).to have_http_status(:ok)
      expect(errors).to be_empty
      expect(vessel).to eq(expected_vessel)
    end
  end

  shared_examples_for 'returns an error' do |message|
    it "with message: #{message.truncate(15)}" do
      expect(response).to have_http_status(:ok)
      expect(graphql_errors.to_s).to include(message)
    end
  end

  it_behaves_like 'updates the vessel'

  context 'without name parameter' do
    let(:name) {}
    let(:expected_name) { db_vessel.name }

    it_behaves_like 'updates the vessel'
  end

  context 'without vesselType parameter' do
    let(:vessel_type) {}
    let(:expected_vessel_type) { 'STANDARD' }

    it_behaves_like 'updates the vessel'
  end

  context 'without decommissioned parameter' do
    let(:decommissioned) {}
    let(:expected_decommissioned) { false }

    it_behaves_like 'updates the vessel'
  end

  context 'when decommissioning' do
    let(:decommissioned) { true }

    it_behaves_like 'updates the vessel'
  end

  context 'with invalid name parameter' do
    let(:name) { 'Name' * 200 }

    it_behaves_like 'returns an error', 'Validation failed: Name'
  end

  context 'with invalid vessel_type parameter' do
    let(:vessel_type) { 'SOMETHING_ELSE' }

    it_behaves_like 'returns an error', 'type VesselTypeEnum was provided invalid value'
  end

  context 'with invalid vesselId parameter' do
    let(:vessel_id) { create(:vessel).id.to_s }

    it_behaves_like 'returns an error', "Couldn't find Vessel with 'id'"
  end

  context 'without logged-in user' do
    let(:do_request) { post_unauthenticated_graphql(variables) }

    it_behaves_like 'returns an error', 'field requires authentication'
  end
end
