# frozen_string_literal: true

require 'spec_helper'

RSpec.describe 'Trash serving mutation' do
  include GraphqlHelpers

  let_it_be(:user) { create(:user) }
  let_it_be(:tea) { create(:tea, household: user.household) }
  let_it_be(:db_serving, reload: true) { create(:serving, tea: tea) }

  let(:serving_id) { db_serving.id.to_s }

  let(:fields) do
    {
      servingId: 'ID!'
    }.slice(*variables.keys)
  end
  let(:variables) do
    {
      servingId: serving_id
    }.compact
  end
  let(:query) do
    <<-GRAPHQL
    mutation#{mutation_input_format(fields)} {
      trashServing#{mutation_input(variables.keys)} {
        serving {
          id
          isFinished
        }
      }
    }
    GRAPHQL
  end

  let(:do_request) { post_authenticated_graphql(variables) }
  let(:serving) { response.parsed_body['data']['trashServing']['serving'] }
  let(:errors) { response.parsed_body['errors'].to_s }
  let(:expected_serving) do
    {
      'id' => serving_id,
      'isFinished' => true
    }
  end

  before do
    do_request
  end

  shared_examples_for 'updates the serving' do
    it 'successfully' do
      expect(response).to have_http_status(:ok)
      expect(errors).to be_empty
      expect(serving).to eq(expected_serving)
    end
  end

  shared_examples_for 'returns an error' do |message|
    it "with message: #{message.truncate(15)}" do
      expect(response).to have_http_status(:ok)
      expect(graphql_errors.to_s).to include(message)
    end
  end

  it_behaves_like 'updates the serving'

  context 'for a trash serving' do
    before do
      db_serving.update!(is_finished: true)
    end

    it_behaves_like 'updates the serving'
  end

  context 'with invalid servingId parameter' do
    let(:serving_id) { create(:serving).id.to_s }

    it_behaves_like 'returns an error', "Couldn't find Serving with 'id'"
  end

  context 'without logged-in user' do
    let(:do_request) { post_unauthenticated_graphql(variables) }

    it_behaves_like 'returns an error', 'field requires authentication'
  end
end
