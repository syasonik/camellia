# frozen_string_literal: true

require 'spec_helper'

RSpec.describe 'Query for vessels' do
  include GraphqlHelpers

  let_it_be(:user) { create(:user) }
  let_it_be(:vessel1) { create(:vessel, :with_serving, household: user.household) }
  let_it_be(:vessel2) { create(:vessel, :decommissioned, household: user.household) }
  let_it_be(:vessel3) { create(:vessel, vessel_type: :niche, household: user.household) }
  let_it_be(:vessel4) { create(:vessel) }
  let_it_be(:serving) { vessel1.serving }
  let_it_be(:steeping) { create(:steeping, serving: serving) }

  let(:params) {}
  let(:vessels) { response.parsed_body['data']['vessels'] }
  let(:errors) { response.parsed_body['errors'].to_s }
  let(:query) do
    <<-GRAPHQL
    query {
      vessels#{params} {
        id
        name
        decommissioned
        vesselType
        serving {
          id
          isFinished
          expectedNextDuration
          expectedNextTemperature
          expectedNextSteepNumber
          tea { id }
          vessel { id }
          steepings {
            id
            serving { id }
            temperature
            duration
            steepNumber
            isGood
          }
        }
      }
    }
    GRAPHQL
  end

  let(:do_request) { post_authenticated_graphql }

  before do
    do_request
  end

  let(:expected_serving) do
    {
      'expectedNextDuration' => steeping.duration,
      'expectedNextTemperature' => steeping.temperature,
      'expectedNextSteepNumber' => 2,
      'id' => serving.id.to_s,
      'isFinished' => serving.is_finished,
      'tea' => { 'id' => serving.tea.id.to_s },
      'vessel' => { 'id' => vessel1.id.to_s },
      'steepings' => [
        {
          'id' => steeping.id.to_s,
          'serving' => { 'id' => serving.id.to_s },
          'temperature' => steeping.temperature,
          'duration' => steeping.duration,
          'steepNumber' => steeping.steep_number,
          'isGood' => steeping.is_good
        },
      ]
    }
  end

  it 'includes all available standard vessels for household' do
    expect(response).to have_http_status(:ok)
    expect(errors).to be_empty
    expect(vessels).to eq(
      [
        {
          'id' => vessel1.id.to_s,
          'name' => vessel1.name,
          'decommissioned' => false,
          'vesselType' => 'STANDARD',
          'serving' => expected_serving
        },
      ]
    )
  end

  context 'with :decommissioned param' do
    let(:params) { '(decommissioned: null)' }

    it 'includes standard decommissioned vessels' do
      expect(response).to have_http_status(:ok)
      expect(errors).to be_empty
      expect(vessels).to eq(
        [
          {
            'id' => vessel1.id.to_s,
            'name' => vessel1.name,
            'decommissioned' => false,
            'vesselType' => 'STANDARD',
            'serving' => expected_serving
          },
          {
            'id' => vessel2.id.to_s,
            'name' => vessel2.name,
            'decommissioned' => true,
            'vesselType' => 'STANDARD',
            'serving' => nil
          },
        ]
      )
    end
  end

  context 'with true :decommissioned param' do
    let(:params) { '(decommissioned: true)' }

    it 'includes only standard decommissioned teas' do
      expect(response).to have_http_status(:ok)
      expect(vessels).to eq(
        [
          {
            'id' => vessel2.id.to_s,
            'name' => vessel2.name,
            'decommissioned' => true,
            'vesselType' => 'STANDARD',
            'serving' => nil
          },
        ]
      )
    end
  end

  context 'with :id param' do
    let(:params) {  "(id: \"#{vessel1.id}\")" }

    it 'includes only the specified vessel' do
      expect(response).to have_http_status(:ok)
      expect(vessels).to eq(
        [
          {
            'id' => vessel1.id.to_s,
            'name' => vessel1.name,
            'decommissioned' => false,
            'vesselType' => 'STANDARD',
            'serving' => expected_serving
          },
        ]
      )
    end
  end

  context 'with null :vesselTypes param' do
    let(:params) { '(vesselTypes: null)' }

    it 'includes available vessels of all types' do
      expect(response).to have_http_status(:ok)
      expect(vessels).to eq(
        [
          {
            'id' => vessel1.id.to_s,
            'name' => vessel1.name,
            'decommissioned' => false,
            'vesselType' => 'STANDARD',
            'serving' => expected_serving
          },
          {
            'id' => vessel3.id.to_s,
            'name' => vessel3.name,
            'decommissioned' => false,
            'vesselType' => 'NICHE',
            'serving' => nil
          },
        ]
      )
    end
  end

  context 'with specified :vesselTypes param' do
    let(:params) { '(vesselTypes: [NICHE])' }

    it 'includes only the specified vessel types' do
      expect(response).to have_http_status(:ok)
      expect(vessels).to eq(
        [
          {
            'id' => vessel3.id.to_s,
            'name' => vessel3.name,
            'decommissioned' => false,
            'vesselType' => 'NICHE',
            'serving' => nil
          },
        ]
      )
    end
  end

  context 'with all :vesselTypes params' do
    let(:params) { '(vesselTypes: [STANDARD, NICHE])' }

    it 'includes available vessels of all types' do
      expect(response).to have_http_status(:ok)
      expect(vessels).to eq(
        [
          {
            'id' => vessel1.id.to_s,
            'name' => vessel1.name,
            'decommissioned' => false,
            'vesselType' => 'STANDARD',
            'serving' => expected_serving
          },
          {
            'id' => vessel3.id.to_s,
            'name' => vessel3.name,
            'decommissioned' => false,
            'vesselType' => 'NICHE',
            'serving' => nil
          },
        ]
      )
    end
  end

  context 'without logged-in user' do
    let(:do_request) { post_unauthenticated_graphql }

    it 'returns an error' do
      expect(graphql_errors.to_s).to include('field requires authentication')
    end
  end
end
