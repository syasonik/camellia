# frozen_string_literal: true

require 'spec_helper'

RSpec.describe 'Query for teas' do
  include GraphqlHelpers

  let_it_be(:user) { create(:user) }
  let_it_be(:tea1) { create(:tea, household: user.household) }
  let_it_be(:tea2) { create(:tea, :decommissioned, household: user.household) }
  let_it_be(:tea3) { create(:tea) }

  let(:params) {}
  let(:teas) { response.parsed_body['data']['teas'] }
  let(:errors) { response.parsed_body['errors'].to_s }
  let(:query) do
    <<-GRAPHQL
    query {
      teas#{params} {
        id
        name
        description
        decommissioned
      }
    }
    GRAPHQL
  end

  let(:do_request) { post_authenticated_graphql }

  before do
    do_request
  end

  it 'includes all available teas for household' do
    expect(response).to have_http_status(:ok)
    expect(teas).to eq(
      [
        {
          'id' => tea1.id.to_s,
          'name' => tea1.name,
          'description' => tea1.description,
          'decommissioned' => false
        },
      ]
    )
  end

  context 'with null :decommissioned param' do
    let(:params) { '(decommissioned: null)' }

    it 'includes available and decommissioned teas' do
      expect(response).to have_http_status(:ok)
      expect(teas).to eq(
        [
          {
            'id' => tea1.id.to_s,
            'name' => tea1.name,
            'description' => tea1.description,
            'decommissioned' => false
          },
          {
            'id' => tea2.id.to_s,
            'name' => tea2.name,
            'description' => tea2.description,
            'decommissioned' => true
          },
        ]
      )
    end
  end

  context 'with true :decommissioned param' do
    let(:params) { '(decommissioned: true)' }

    it 'includes only decommissioned teas' do
      expect(response).to have_http_status(:ok)
      expect(teas).to eq(
        [
          {
            'id' => tea2.id.to_s,
            'name' => tea2.name,
            'description' => tea2.description,
            'decommissioned' => true
          },
        ]
      )
    end
  end

  context 'with id param' do
    let(:params) { "(id: \"#{tea1.id}\")" }

    it 'includes only the specified tea' do
      expect(response).to have_http_status(:ok)
      expect(teas).to eq(
        [
          {
            'id' => tea1.id.to_s,
            'name' => tea1.name,
            'description' => tea1.description,
            'decommissioned' => false
          },
        ]
      )
    end
  end

  context 'without logged-in user' do
    let(:do_request) { post_unauthenticated_graphql }

    it 'returns an error' do
      expect(graphql_errors.to_s).to include('field requires authentication')
    end
  end
end
