# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Serving, type: :model do
  let(:user) { create(:user, household: serving.tea.household) }

  subject(:serving) { create(:serving) }

  describe 'associations' do
    it { is_expected.to belong_to(:tea) }
    it { is_expected.to belong_to(:vessel) }
    it { is_expected.to have_many(:steepings) }
  end

  describe 'validations' do
    context 'when in progress' do
      subject { create(:serving, is_finished: false) }

      it do
        is_expected
          .to validate_uniqueness_of(:vessel)
          .scoped_to(:is_finished)
          .with_message('should only have one unfinished serving')
      end
    end

    context 'when finished' do
      subject { create(:serving, :is_finished) }

      it { is_expected.not_to validate_uniqueness_of(:vessel).scoped_to(:is_finished) }
    end
  end

  describe '#recommended_steep_params' do
    subject { serving.recommended_steep_params }

    context 'with an escalation profile' do
      let(:tea) { create(:tea, :with_escalation) }
      let(:serving) { create(:serving, tea: tea) }
      let(:next_escalation) { tea.escalation_profiles.first.escalations.first }

      it 'uses the next escalation' do
        expect(subject).to eq(
          duration: next_escalation.duration,
          temperature: next_escalation.temperature,
          steep_number: 1
        )
      end

      # TODO: Consider adding specs for when escalation profile
      # does not have any escalations

      context 'with steepings exceeding escalation profile' do
        let(:last_steep) { Steeping.last }

        before do
          3.times do
            Steepings::CreateService.new(user, serving_id: serving.id).execute
          end
        end

        it 'uses the next escalation' do
          expect(subject).to eq(
            duration: last_steep.duration,
            temperature: last_steep.temperature,
            steep_number: 4
          )
        end
      end
    end

    context 'without escalation profile' do
      it 'does not provide estimatel values' do
        expect(subject).to eq(
          duration: described_class::DEFAULT_STEEP_DURATION,
          temperature: described_class::DEFAULT_STEEP_TEMPERATURE,
          steep_number: 1
        )
      end

      context 'with steepings' do
        before do
          Steepings::CreateService.new(user, serving_id: serving.id, duration: 60, temperature: 100).execute
        end

        it 'uses the last steeping' do
          expect(subject).to eq(
            duration: 60,
            temperature: 100,
            steep_number: 2
          )
        end
      end
    end
  end

  describe '#in_progress?' do
    subject { serving.in_progress? }

    context 'when finished' do
      let(:serving) { create(:serving, :is_finished) }

      it { is_expected.to be(false) }
    end

    context 'when in progress' do
      it { is_expected.to be(true) }
    end
  end

  describe '#trash!' do
    subject { serving.trash! }

    it 'clears the vessel and finishes the serving' do
      subject

      expect(serving.vessel.reload.serving).to be_nil
      expect(serving.reload).not_to be_in_progress
    end

    context 'when vessel is unoccupied' do
      before do
        serving.vessel.set_serving!(serving)
      end

      it 'clears the vessel and finishes the serving' do
        subject

        expect(serving.vessel.reload.serving).to be_nil
        expect(serving.reload).not_to be_in_progress
      end
    end
  end
end
