# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Vessel, type: :model do
  subject(:vessel) { create(:vessel) }

  describe 'associations' do
    it { is_expected.to belong_to(:serving).optional }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_length_of(:name).is_at_most(255) }
    it { is_expected.to validate_uniqueness_of(:name).scoped_to(:household_id) }

    it { is_expected.to validate_presence_of(:vessel_type) }
  end

  describe '#set_serving!' do
    let!(:serving) { create(:serving) }

    subject { vessel.set_serving!(serving) }

    it 'assigns the vessel to a serving' do
      subject

      expect(vessel.reload.serving).to eq(serving)
    end
  end

  describe '#empty!' do
    subject { vessel.empty! }

    context 'when there is no serving' do
      it 'makes no changes' do
        subject

        expect(vessel.reload.serving).to be_nil
      end
    end

    context 'when there is a serving' do
      let(:vessel) { create(:vessel, :with_serving) }

      it 'unassigns the vessel from any servings' do
        subject

        expect(vessel.reload.serving).to be_nil
      end
    end
  end

  describe '#decommission!' do
    subject { vessel.decommission! }

    it 'takes the vessel out of rotation' do
      subject

      expect(vessel).to be_decommissioned
    end

    context 'when there is a serving' do
      let(:vessel) { create(:vessel, :with_serving) }

      it 'preserves the serving' do
        subject

        expect(vessel.reload.serving).to be_present
        expect(vessel).to be_decommissioned
      end
    end
  end
end
