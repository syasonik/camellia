# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Tea, type: :model do
  subject(:tea) { create(:tea) }

  describe 'associations' do
    it { is_expected.to have_many(:servings) }
    it { is_expected.to have_many(:steepings) }
    it { is_expected.to have_many(:escalation_profiles) }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_length_of(:name).is_at_most(255) }
    it { is_expected.to validate_uniqueness_of(:name).scoped_to(:household_id) }

    it { is_expected.to validate_presence_of(:description).allow_nil }
    it { is_expected.to validate_length_of(:description).is_at_most(2000) }

    it do
      is_expected
        .to validate_numericality_of(:servings_remaining)
        .only_integer
        .is_greater_than_or_equal_to(0).allow_nil
    end
  end

  describe '#escalation_profile' do
    let!(:vessel) { create(:vessel) }
    let!(:default_profile) { create(:escalation_profile, tea: tea) }

    subject { tea.escalation_profile(vessel) }

    it { is_expected.to eq default_profile }

    context 'when vessel-specific profiles exist by are not found' do
      let!(:other_vessel_profile) { create(:escalation_profile, :with_vessel, tea: tea) }

      it { is_expected.to eq default_profile }
    end

    context 'when a vessel-specific profile is available' do
      let!(:vessel_profile) { create(:escalation_profile, vessel: vessel, tea: tea) }

      it { is_expected.to eq vessel_profile }
    end
  end

  describe '#use_serving!' do
    subject { tea.use_serving! }

    context 'without expected servings value' do
      specify { expect { subject }.not_to change(tea, :servings_remaining) }
    end

    context 'with servings available' do
      let(:tea) { create(:tea, servings_remaining: 3) }

      specify { expect { subject }.to change(tea, :servings_remaining).by(-1) }
    end

    context 'with 0 servings available' do
      let(:tea) { create(:tea, servings_remaining: 0) }

      specify { expect { subject }.not_to change(tea, :servings_remaining) }
    end
  end

  describe '#decommission!' do
    subject { tea.decommission! }

    context 'when tea is available' do
      specify { expect { subject }.to change(tea, :decommissioned?).from(false).to(true) }
    end

    context 'when tea is already decommissioned' do
      let(:tea) { create(:tea, :decommissioned) }

      specify { expect { subject }.not_to change(tea, :decommissioned?) }
    end
  end
end
