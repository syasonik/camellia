# frozen_string_literal: true

Rails.application.routes.draw do
  mount GraphiQL::Rails::Engine, at: '/graphiql', graphql_path: '/graphql'

  post '/graphql', to: 'graphql#execute'

  get '/', to: 'application#not_found'
  get '*unmatched_route', to: 'application#not_found'
end
