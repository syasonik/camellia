# frozen_string_literal: true

module GraphqlDevise
  class DetailedUserError < ExecutionError
    def initialize(message, errors:)
      @message = errors.prepend(message).join('. ').concat('.')

      super(@message)
    end

    def to_h
      super.merge(extensions: { code: ERROR_CODES.fetch(:user_error) })
    end
  end
end
