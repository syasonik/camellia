# frozen_string_literal: true

require 'active_record/connection_adapters/postgresql_adapter'

module ActiveRecord
  module ConnectionAdapters
    class PostgreSQLAdapter
      def native_database_types
        NATIVE_DATABASE_TYPES.merge(datetime: { name: 'timestamptz' })
      end
    end
  end
end
