# Authentication

User authentication is managed through GraphQL using `devise_graphql`, which relies on `devise-auth-token` & `devise`.

Authentication flow:

1. User signs up for an account or logs in via a GraphQL mutation.
2. Response includes content which populate headers of subsequent requests.
3. User signs out via GraphQL mutation.

See [devise_graphql docs](https://github.com/graphql-devise/graphql_devise#mutations) for all available mutations. Mutations related to email confirmation are not included.

### Sample Signup Mutation

```graphql
mutation {
	userSignUp(
    email: "foo@bar.com",
    password: "password123",
    passwordConfirmation: "password123"
  ) {
    credentials {
      accessToken
    }
  }
}
```

### Sample Login Mutation

```graphql
mutation {
  userLogin(
    email: "foo@bar.com",
    password: "password123"
  ) {
    credentials {
      accessToken
      uid
      tokenType
      client
      expiry
    }
  }
}
```

Response:

```json
{
  "data": {
    "userLogin": {
      "credentials": {
        "accessToken": "MuT5h43iiRVIAHIa4kNEig",
        "uid": "foo@bar.com",
        "tokenType": "Bearer",
        "client": "fdbaSAn5E-G8B7N7SxOEQw",
        "expiry": 1602969118
      }
    }
  }
}
```

### Sample Authenticated Request
```graphql
{
  teas {
    servings {
      steepings {
        id
        isGood
      }
    }
  }
  vessels {
    name
  }
}      
```

Request headers should be filled corresponding to response attributes from login request, like so:

```shell
content-type: application/json
access-token: MuT5h43iiRVIAHIa4kNEig
token-type: Bearer
client: fdbaSAn5E-G8B7N7SxOEQw
expiry: 1602969118
uid: foo@bar.com
```
