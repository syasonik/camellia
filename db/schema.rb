# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_10_28_215954) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "escalation_profiles", force: :cascade do |t|
    t.bigint "tea_id", null: false
    t.bigint "vessel_id"
    t.index ["tea_id", "vessel_id"], name: "index_escalation_profiles_on_tea_id_and_vessel_id", unique: true, where: "(vessel_id IS NOT NULL)"
    t.index ["tea_id"], name: "index_escalation_profiles_on_tea_id"
    t.index ["vessel_id"], name: "index_escalation_profiles_on_vessel_id"
  end

  create_table "escalations", force: :cascade do |t|
    t.integer "rank", null: false
    t.integer "duration", null: false
    t.integer "temperature", null: false
    t.bigint "escalation_profile_id", null: false
    t.index ["escalation_profile_id"], name: "index_escalations_on_escalation_profile_id"
    t.index ["rank", "escalation_profile_id"], name: "index_escalations_on_rank_and_escalation_profile_id", unique: true
  end

  create_table "households", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "servings", force: :cascade do |t|
    t.bigint "tea_id", null: false
    t.bigint "vessel_id", null: false
    t.boolean "is_finished", default: false
    t.index ["tea_id", "vessel_id"], name: "index_servings_on_tea_id_and_vessel_id", unique: true, where: "(is_finished IS FALSE)"
    t.index ["tea_id"], name: "index_servings_on_tea_id"
    t.index ["vessel_id"], name: "index_servings_on_vessel_id"
  end

  create_table "steepings", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "serving_id", null: false
    t.integer "duration"
    t.integer "temperature"
    t.integer "steep_number"
    t.boolean "is_good"
    t.index ["serving_id"], name: "index_steepings_on_serving_id"
  end

  create_table "teas", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name", null: false
    t.text "description"
    t.integer "servings_remaining"
    t.boolean "decommissioned", default: false, null: false
    t.bigint "household_id"
    t.index ["household_id"], name: "index_teas_on_household_id"
    t.index ["name", "household_id"], name: "index_teas_on_name_and_household_id", unique: true
  end

  create_table "users", force: :cascade do |t|
    t.string "provider", default: "email", null: false
    t.string "uid", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.boolean "allow_password_change", default: false
    t.datetime "remember_created_at"
    t.string "name"
    t.string "nickname"
    t.string "image"
    t.string "email"
    t.json "tokens"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "household_id"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["household_id"], name: "index_users_on_household_id"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["uid", "provider"], name: "index_users_on_uid_and_provider", unique: true
  end

  create_table "vessels", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.integer "vessel_type", null: false
    t.boolean "decommissioned", default: false, null: false
    t.bigint "serving_id"
    t.bigint "household_id"
    t.index ["household_id"], name: "index_vessels_on_household_id"
    t.index ["name", "household_id"], name: "index_vessels_on_name_and_household_id", unique: true
    t.index ["serving_id"], name: "index_vessels_on_serving_id"
  end

  add_foreign_key "escalation_profiles", "teas", on_delete: :cascade
  add_foreign_key "escalation_profiles", "vessels", on_delete: :cascade
  add_foreign_key "escalations", "escalation_profiles", on_delete: :cascade
  add_foreign_key "servings", "teas", on_delete: :cascade
  add_foreign_key "servings", "vessels", on_delete: :cascade
  add_foreign_key "steepings", "servings", on_delete: :cascade
end
