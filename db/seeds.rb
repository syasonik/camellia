# frozen_string_literal: true

# rubocop:disable Lint/UselessAssignment
# rubocop:disable Layout/LineLength
user = User.create(email: 'foo@bar.com', password: 'password123', password_confirmation: 'password123')
household = user.household

formosa = Tea.create!(name: 'formosa 18', description: 'super awesome red blossom tea', decommissioned: true, household: household)
cultivar = Tea.create!(name: 'native cultivar', description: 'super awesome red blossom tea', household: household)
lulo = Tea.create!(name: 'lulo', description: 'colombian green tea', household: household)

travel_strainer = Vessel.create!(name: 'travel strainer', vessel_type: 'standard', household: household)
strainer = Vessel.create!(name: '4Life', vessel_type: 'standard', household: household)
stick = Vessel.create!(name: 'stick strainer', vessel_type: 'niche', household: household)
little_pot = Vessel.create!(name: 'little pot', vessel_type: 'niche', household: household)
big_strainer = Vessel.create!(name: 'big strainer', vessel_type: 'niche', decommissioned: true, household: household)

formosa_serving = Serving.create!(vessel: travel_strainer, tea: formosa)
travel_strainer.update!(serving: formosa_serving)

Steeping.create!(serving: formosa_serving, duration: 150, temperature: 90, steep_number: 1, is_good: true)
Steeping.create!(serving: formosa_serving, duration: 180, temperature: 90, steep_number: 2, is_good: false)

cultivar_serving = Serving.create!(vessel: big_strainer, tea: cultivar)
big_strainer.update!(serving: cultivar_serving)

Steeping.create!(serving: cultivar_serving, duration: 150, temperature: 100, steep_number: 1, is_good: true)
Steeping.create!(serving: cultivar_serving, duration: 180, temperature: 100, steep_number: 2, is_good: true)

standard_profile = EscalationProfile.create!(tea: cultivar)
Escalation.create!(rank: 1, temperature: 90, duration: 150, escalation_profile: standard_profile)
Escalation.create!(rank: 2, temperature: 90, duration: 180, escalation_profile: standard_profile)
Escalation.create!(rank: 3, temperature: 100, duration: 210, escalation_profile: standard_profile)
Escalation.create!(rank: 4, temperature: 100, duration: 900, escalation_profile: standard_profile)

pot_profile = EscalationProfile.create!(tea: cultivar, vessel: little_pot)
Escalation.create!(rank: 1, temperature: 90, duration: 30, escalation_profile: pot_profile)
Escalation.create!(rank: 2, temperature: 90, duration: 45, escalation_profile: pot_profile)
Escalation.create!(rank: 3, temperature: 100, duration: 60, escalation_profile: pot_profile)
Escalation.create!(rank: 4, temperature: 100, duration: 90, escalation_profile: pot_profile)
Escalation.create!(rank: 5, temperature: 100, duration: 120, escalation_profile: pot_profile)

formosa_profile = EscalationProfile.create!(tea: formosa)
Escalation.create!(rank: 1, temperature: 90, duration: 150, escalation_profile: formosa_profile)
Escalation.create!(rank: 2, temperature: 90, duration: 180, escalation_profile: formosa_profile)
Escalation.create!(rank: 3, temperature: 100, duration: 210, escalation_profile: formosa_profile)
Escalation.create!(rank: 4, temperature: 100, duration: 900, escalation_profile: formosa_profile)

lulo_profile = EscalationProfile.create!(tea: lulo)
Escalation.create!(rank: 1, temperature: 90, duration: 120, escalation_profile: lulo_profile)
Escalation.create!(rank: 2, temperature: 90, duration: 180, escalation_profile: lulo_profile)
Escalation.create!(rank: 3, temperature: 90, duration: 300, escalation_profile: lulo_profile)

# rubocop:enable Lint/UselessAssignment
# rubocop:enable Layout/LineLength
