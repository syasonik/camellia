# frozen_string_literal: true

class AddServingToVessels < ActiveRecord::Migration[5.2]
  def change
    add_reference :vessels, :serving
  end
end
