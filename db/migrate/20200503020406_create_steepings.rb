# frozen_string_literal: true

class CreateSteepings < ActiveRecord::Migration[5.2]
  def change
    create_table :steepings do |t|
      t.timestamps
      t.references :serving, foreign_key: { on_delete: :cascade }, null: false
      t.integer :duration
      t.integer :temperature
      t.integer :steep_number
      t.boolean :is_good
    end
  end
end
