# frozen_string_literal: true

class CreateTeas < ActiveRecord::Migration[5.2]
  def change
    create_table :teas do |t|
      t.timestamps
      t.string :name, null: false, unique: true
      t.text :description
      t.integer :servings_remaining
      t.boolean :decommissioned, default: false, null: false
    end
  end
end
