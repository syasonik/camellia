# frozen_string_literal: true

class CreateEscalationProfiles < ActiveRecord::Migration[5.2]
  def change
    create_table :escalation_profiles do |t|
      t.references :tea, foreign_key: { on_delete: :cascade }, null: false
      t.references :vessel, foreign_key: { on_delete: :cascade }
    end

    add_index(:escalation_profiles, %i[tea_id vessel_id], unique: true, where: 'vessel_id is NOT NULL')
  end
end
