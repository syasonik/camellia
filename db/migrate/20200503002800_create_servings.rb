# frozen_string_literal: true

class CreateServings < ActiveRecord::Migration[5.2]
  def change
    create_table :servings do |t|
      t.references :tea, foreign_key: { on_delete: :cascade }, null: false
      t.references :vessel, foreign_key: { on_delete: :cascade }, null: false
      t.boolean :is_finished, default: false
    end

    add_index(:servings, %i[tea_id vessel_id], unique: true, where: '(is_finished IS FALSE)')
  end
end
