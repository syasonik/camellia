# frozen_string_literal: true

class CreateEscalations < ActiveRecord::Migration[5.2]
  def change
    create_table :escalations do |t|
      t.integer :rank, null: false
      t.integer :duration, null: false
      t.integer :temperature, null: false
      t.references :escalation_profile, foreign_key: { on_delete: :cascade }, null: false
    end

    add_index(:escalations, %i[rank escalation_profile_id], unique: true)
  end
end
