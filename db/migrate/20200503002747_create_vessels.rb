# frozen_string_literal: true

class CreateVessels < ActiveRecord::Migration[5.2]
  def change
    create_table :vessels do |t|
      t.timestamps
      t.string :name, unique: true
      t.integer :vessel_type, null: false
      t.boolean :decommissioned, default: false, null: false
    end
  end
end
