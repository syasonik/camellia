# frozen_string_literal: true

class CreateHouseholds < ActiveRecord::Migration[5.2]
  def change
    create_table :households do |t| # rubocop:disable Style/SymbolProc
      t.timestamps
    end

    add_reference :teas, :household
    add_reference :vessels, :household
    add_reference :users, :household

    add_index(:teas, %i[name household_id], unique: true)
    add_index(:vessels, %i[name household_id], unique: true)
  end
end
